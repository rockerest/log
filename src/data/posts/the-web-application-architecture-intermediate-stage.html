---{
		"id": "c749b165-6fd6-4e73-be29-21590487c282",
    "authors": [
        "trandolph"
    ],
    "title": "The Web Application Architecture - Intermediate Stage",
    "publish": "2024-01-22T21:13:00-07:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
    <h2>Part 2: Solving Part 1</h2>
    <p>
      In order to discuss more intermediate topics of the proper architecture for a web application, we have to operate on the assumption that your application has already accomplished <a href="/post/the-web-application-architecture.html">the fundamentals</a>.
    </p>
    <p>
        The problem is that the current frog-in-a-pot front end ecosystem has convinced most developers and - by extension - their companies that they <em>have</em> accomplished the fundamentals, and they're ready to move on to more advanced topics. I don't believe this is remotely true. However, I'm not your dad, so I can't stop you from reading this.
        <br />
        Consider, though, that if your development environment has a mandatory build system, or if any of your source code has specific signaling just for your bundler, or if you use React, Vue, Apollo, or any Flux implementation, you cannot possibly have accomplished even <strong>one</strong> of the three items necessary for the fundamentals of proper web application architecture. These are by no means the full list of tools and practices, but they are certainly some of the most popular.
    </p>
    <p>
        That statement probably causes a visceral reaction in you, perhaps even disgust and anger. I am genuinely sorry to make you feel this way. Unfortunately, the level to which web developers have incorporated the <em>tooling that has built up in their toolbelt over time</em> into the core of their <em>identities</em> is alarming, and we can't keep dancing around the fact that the tooling is a huge part of the problem.
        <br />
        If you've incorporated these tools into your identity, you are <strong>not the problem</strong>! You are valuable and useful! 🙂 But we do have to look at the cold truth about our tooling, processes, and architectures.
    </p>
    <p>
      So: read on - nobody can stop you. Just know that if you don't truly nail the <a href="/post/the-web-application-architecture.html">fundamentals</a>, anything else you do will be built on a foundation of sand.
    </p>
  </section>
  <section>
    <h2>It's the fun part</h2>
    <p>
        The silver lining here is that once you've nailed the fundamentals of de-complexity, robust foundations, and inter- & intra-app communication, everything else becomes much easier.
        <br />
        Removing complexity and establishing a standard way everything "talks" removes much of the difficulty of removing, adding, and upgrading things. You're then left to work on the actual problems that further your software's goals.
        <br />
        <em>But wait!</em> you exclaim, <em>my tooling also allows me to focus on my core business problems instead of all the other junk!</em>
        <br />
        But does it? In every "bog-standard" ecosystem I've worked in, developers almost never solved novel problems or dramatically improved overall application performance or created uniquely elegant solutions to dramatically improve customer experience.
        <br />
        Almost without fail, most developers spend the vast majority of their time <em>fitting pre-defined solutions to inflexible libraries and systems.</em>
    </p>
    <p>
        So when you escape from that cycle - the one where everything gets worse and worse over time until you can't recover it any more and you either leave the company or are forced into a full rewrite - web software development gets fun again.
    </p>
    <p>
        In <a href="/post/the-web-application-architecture.html">the first part</a>, I was intentionally vague about how to solve these problems. It's possible the right solution for your situation doesn't look like the solution for mine.<br />
        However, architecture is about blueprints, so here's one with more specifics.
    </p>
  </section>
  <section>
    <h2>There will always be software tools</h2>
    <p>
        You could absolutely 100% roll your own solutions to every problem, but it's unlikely that you should for the most complex parts of an application.
        <br />
        That complexity should be irreducible (for example: updating the DOM), and not self-inflicted.
    </p>
    <h2>There must always be systems</h2>
    <p>
        The original sin of most of the terrible software in modern web development is that they are tools that try to solve problems that can only be solved by systems.
        <br />
        What's the difference?
        <br />
        Tools plug into specific parts of systems. Systems are ideas. You can encapsulate a system purely in documentation. You should probably also have some code that implements the system, but it isn't technically necessary.
    </p>
  </section>
  <section>
    <h2>De-complexifying systems</h2>
    <p>
        Here it is: <em>What does de-complexifying my application look like?</em>
    </p>
    <ul>
        <li>Anything that solves more than one problem at a time is too complex.</li>
        <li>Anything that deviates significantly from your platform is too complex.</li>
    </ul>
    <p>
        It should be pretty clear why those statements are true, but here's a quick explainer.
        <br />
        If something solves two problems at the same time ("problem" here are classes of application-level concerns, like say: requesting information from an API, request caching, and data binding), any issue with one problem becomes something that affects <strong>all</strong> of the other problems.
        <br />
        You have an issue with how data binding into your components work? <strong>Now you also have a problem with how network requests to your API work.</strong> That intertwinedness should actively raise your blood pressure.
        <br />
        In similar fashion, when something deviates significantly from your platform, you become less able to adapt to underlying changes to that platform.
        This is the problem with all of the VDOM frameworks in use today: they deviated significantly from the platform.
        The platform has a native component model and they're unable to even come close to the performance or flexibility provided by the platform.
        <br />
        Experienced engineers warned of this a decade or more ago. Code written in defiance of platform norms is zombie code: already dead and a danger to everything around it.
    </p>
  </section>
  <section>
    <h2>Simple front end application structure</h2>
    <p>
      Recall that simple is not the same as easy. Simple systems separate from each other so that they can interoperate <strong>more</strong> seamlessly by agreeing on standardized interfaces. This allows them to act as discrete black-box systems instead of having their internals deeply intertwined.
    </p>
    <h3>Generic</h3>
    <p>
      You could break most web applications down into these four (really three) parts:
      <figure>
        <a href="https://res.cloudinary.com/rockerest/image/upload/v1705356434/basic_glmfmz.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705356434/basic_glmfmz.png" alt="Four green rectangles are in a horizontal line with some space between each. The are labeled, in order, 'Data', 'Business Logic', 'Display Logic', and 'UI'. The rectangles are connected with arrows. 'Data' has a double-headed arrow connecting to 'Business Logic'. 'Business Logic' is connected with a single-head arrow pointing to 'Display Logic'. 'Display Logic' is connected with a single-head arrow pointing to 'UI'. 'UI' is connected back to 'Display Logic' with a separate single-headed arrow labeled 'UX Reactivity'." /></a>
        <figcaption>A basic web application has three (or four) parts: Data (retrieval, storage), Business Logic, and Display Logic (which results in part four: the UI). The UI sends signals back to the display logic based on user input.</figcaption>
      </figure>
      <aside>While not crucial to the discussion here, note that the UI is an artifact of the display logic. These two items are naturally very tightly coupled. <strong>How</strong> and <strong>when</strong> a UI should display are very nearly inextricably bound up with <strong>what</strong> should display.</aside>
    </p>
    <h3>Specific</h3>
    <p>
      It could help to better define these parts of the application, so we can rename each more precisely:
      <figure>
        <a href="https://res.cloudinary.com/rockerest/image/upload/v1705356435/specific_uzkjhu.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705356435/specific_uzkjhu.png" alt="The four green rectangles from previously now have updated labels. Respectively, they are: 'Data', 'Application Value Proposition', 'Pages, Components, & Component State', and 'DOM'. Additionally, the arrow between 'Application Value Proposition' and 'Pages, Components, & Component State' is double-headed. In a more generic system, 'Business Logic' would unidirectionally drive 'Display Logic', but with this more specific outline, 'Pages, Components, & Component State' should likely have a feedback loop to the 'Application Value Proposition'." /></a>
        <figcaption>A more precise web application has four parts: Data; the Application's Value Proposition (why does it exist?); Pages, Components, & Component State; and DOM. The DOM sends signals back to the component or page based on user input.</figcaption>
      </figure>
    </p>
    <h3>Specifically correct (separation of the parts)</h3>
    <p>
      In these images, each part of the application is connected directly to another part. This type of connection is how most applications today are built, and it encourages deeply broken interactions and spaghetti code. This is not only because there aren't clear boundaries about where things should go, but also because it <strong>fundamentally doesn't matter</strong>: the application's UI is virtually indistinguishable from its core value proposition. If you put application logic directly in a UI component, it doesn't matter, because the UI <strong>is</strong> the business logic.
      <br />
      This is - of course - madness (See <em>De-complexifying systems</em>, just above).
      <br />
      A robust, scalable architecture requires that each part of a system operate in a predictable way. To keep predictability to a maximum without complex interdependencies causing literal exponential growth of potential outcomes (see: <a href="https://kyleshevlin.com/enumerate-dont-booleanate/">Enumerate, Don't Booleanate</a> for an exploration of explosive exponential growth of interdependent state on a micro scale - within one component), each part should attach to a standardized interface.
      <br />
      You could call this "Signals" or "Actions" or "Events" or "Messages," but whatever they are, the idea is that the application has a backbone of messages passing back and forth, and when a relevant message is noticed, something occurs. Critically, this is the <em>only</em> way the application should operate. Every behavior and data change should be able to be followed through a clear history of messages being passed. I have talked about this <a href="https://log.rdl.ph/post/fluxbus-an-application-architecture-pattern.html">multiple</a> times <a href="https://log.rdl.ph/post/fluxbus-2-2-flux-2-furious.html">before</a>. I also built <a href="https://www.npmjs.com/package/gravlift">gravlift</a> to be this carrier backbone for any system.
       <aside>Yes, "Signals" as defined in the mega-hype JavaScript ecosystem churn cycle are essentially "reactive data" in that you define a <em>value</em> that will then <em>notify</em> subscribers to update. This - once again - conflates data with other concerns (application command & control), but it's par for the course we are on. The concept is broadly the same: you subscribe to an emitter and react to appropriate events. It's just that that structure shouldn't be fundamentally based on data.</aside>
       <figure>
          <a href="https://res.cloudinary.com/rockerest/image/upload/v1705356434/expanded_gnpozy.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705356434/expanded_gnpozy.png" alt="The same four green rectangles as previously are still present, and labeled the same. The two spaces between 'Data', 'Application Value Proposition', and 'Pages, Components, & Component State' are much wider and their connecting arrows are gone. In those spaces, and below the line of horizontal green boxes are two new blue boxes, each labeled 'Messenger'. Each has two double-headed arrows. The two on the left 'Messenger' box connect to 'Data' and 'Application Value Proposition' respectively. The two on the right 'Messenger' box connect to 'Application Value Proposition' and 'Pages, Components, & Component State' respectively. This shows how the standardized messaging interface is the go between 'shuttle' for all application behaviors." /></a>
          <figcaption>To standardize on communication in an application where each part is separate from the others (the goal!) we add a Messenger, which shuttles standardized message packets between the Data, Value Proposition, and Page/Component parts of the application. The DOM remains an artifact of the Page/Component part of the application and does not directly interact with the core structure of the application, only sending reactivity signals back to its parent Page/Component.</figcaption>
       </figure>
    </p>
    <h3>Different types of display logic</h3>
    <p>
      The "Pages, Components, & Component State" section is doing a lot of work, so we can split that up appropriately.
      <figure>
        <a href="https://res.cloudinary.com/rockerest/image/upload/v1705975745/Flow_with_component_page_breakout_ovyumi.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705975745/Flow_with_component_page_breakout_ovyumi.png" alt="The four green rectangles and two blue rectangles from previously have been split up. Everything left of the second blue 'Messenger' rectangle remains the same: 'Data' and 'Application Value Proposition' still connect through the first 'Messenger' box. 'Application Value Proposition' still connects to the second 'Messenger' box. The final two green boxes have been replaced with a set of four new green boxes in a square layout to the right of the second 'Messenger' box. Clockwise from the top left, the new green boxes are 'Components', 'DOM', 'DOM', and 'Pages'. 'Components' and 'Pages' each connect to the second 'Messenger' box with their own double-headed arrow. 'Components' adopts the configuration from the previous image and has a single-headed arrow connecting to the first 'DOM' box. That 'DOM' box has a separate single-headed arrow labeled 'UX Reactivity' that connects back to 'Components'. The 'Pages' box connects to both 'Components' and the second 'DOM' box with unidirectional, single-headed arrows. This indicates that 'Pages' can render their own DOM and other components, but components can't render pages. Only components have UX Reactivity." /></a>
        <figcaption>Pages and components are two sides of a coin, but they have slightly different usecases. Primarily, pages can render components and DOM. Components can only render other components and DOM. Only components describe UX reactivity. DOM is still the artifact of these two classes of display logic.</figcaption>
      </figure>
    </p>
    <h3>Defining data</h3>
    <p>
      For many people, defining data is difficult. This is made especially difficult because many tools improperly conflate the concepts of application state (what is the application doing right now) and core application data (what is the information that any user is here to see). These tools incorrectly treat these two concepts as the same, and store them the same way and provide the same interfaces for accessing and mutating them.
    <br />
    The clear defining line is "If you wouldn't store it in your database, it's not data." Loading indicators, which sections of a page have been collapsed by the user, which temporary view modification is enabled, and more are all examples of data you wouldn't store in your database. These are component-level state concerns and don't belong in data.
    <br />
    Data is a broad abstraction of three separate things. Your application's value proposition should not be aware of these distinctions; that's why the Data interface is separated by the standardized Messenger.
    </p>
    <ul>
      <li>Browser Storage</li>
      <li>Single-Concept Utilities</li>
      <li>API</li>
    </ul>
    <figure>
      <a href="https://res.cloudinary.com/rockerest/image/upload/v1705975744/Breakout_with_data_expansion_perj5r.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705975744/Breakout_with_data_expansion_perj5r.png" alt="The previous image's structure remains. The 'Data' box now has a trident arrow to three purple boxes. They are labeled 'Browser Storage', 'Single-Concept Utilities', and 'API'." /></a>
      <figcaption>The Data interface blends access to a back end API, browser storage (for local caching), and single-concept utilities.</figcaption>
    </figure>
    <p>
      The "single-concept utilities" above requires a bit of explanation. It is very common to need to do certain things with known types of data that should happen based on raw data at this low level of the application. For example, you may be building an app that allows users to view and rate dogs (they are all 12/10). After the application gets data from the API, it may need to import the <code>normalize</code> function from <code>Dog.js</code> and make sure to smooth over any potential rough edges in the data from the API. <code>Dog.js</code> might also provide some helpers like <code>makeNew</code> for when someone is submitting a new dog, and the system needs a "default" Dog value for them to fill in.
      <br />
      The two-fold key is that these utilities are tightly focused (for example, just <code>Dog.js</code>, not <code>normalizeData.js</code>) and they are limited to data. There's a whole part of the app for the value proposition code, this area should just be for unavoidable <a href="https://en.wikipedia.org/wiki/Mung_(computer_term)">data mungeing</a>.
    </p>
    <h3>More elaborate components and component DOM</h3>
    <p>
      Components hold a bit of information (the state of the application, which we covered earlier in that we know it is not data). Component DOM also updates regularly, which means we should define how that happens.
    <figure>
      <a href="https://res.cloudinary.com/rockerest/image/upload/v1705356435/expanded-with-data-and-ui_wgkx4z.png"><img src="https://res.cloudinary.com/rockerest/image/upload/v1705356435/expanded-with-data-and-ui_wgkx4z.png" alt="Everything from the previous image remains intact. A purple box labeled 'Finite State Machine' is connected to 'Components' with a single-headed arrow. Two purple boxes labeled 'Surgical Updates' and 'Web Components' are connected to the Components 'DOM' box with single-headed arrows." /></a>
      <figcaption>Application state is encapsulated in component Finite State Machines so that it can be both catalogued and repeated. DOM updates are done as little as possible - updating only what's necessary, not recomputing and re-rendering the entire tree. Components use the web-native component model.</figcaption>
    </p>
    <h3>A blueprint for long-lasting, high-performing applications</h3>
    <p>
      And so we have arrived at the simple application architecture. Had you followed each of the principles in <a href="/post/the-web-application-architecture.html">part 1</a>, you may have arrived at a similar architecture. It is not necessarily <em>easy</em>, but the best things never are. It is undoubtedly <em>elaborate</em>, but most meaningfully complex web applications are elaborate. In this structure, the complexity is kept from seeping into every corner of the code.
    </p>
    <ol>
      <li>
        The application is split into four discrete parts:
        <ol>
          <li>Data</li>
          <li>the application's Value Proposition (the Business Logic)</li>
          <li>the display logic, split between Pages and Components</li>
          <li>the rendering artifact of that logic: the UI, which returns reactive signals back to the components</li>
        </ol>
      </li>
      <li>
        Communication is passed among the Data, Value Proposition, and Pages/Components by a message system with a standardized format
      </li>
      <li>
        Data reflects database information. It is not application state. The Data application layer abstracts:
        <ul>
          <li>accessing the API</li>
          <li>Browser Storage (local cache, for one example)</li>
          <li>Single-concept utilities, which help normalize and interact with data constructs</li>
        </ul>
      </li>
      <li>
        Components use Finite State Machines to catalogue non-exponential lists of application states, the non-data type of information your application must manage.
      </li>
      <li>
        Component DOM updates are managed by the family of Web Components browser features (like Shadow DOM, custom elements, and constructable stylesheets). These updates are not full-component re-computes (like VDOM libraries), but instead maintain connections to live versus static parts of the template, and only update those parts that have received changes.
      </li>
    </ol>
</section>
