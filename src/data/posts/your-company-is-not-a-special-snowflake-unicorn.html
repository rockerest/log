---{
		"id": "bb1f3545-ff50-4602-9c7b-d9d0987355dc",
    "authors": [
        "trandolph"
    ],
    "title": "Your Company is Not a Special Snowflake Unicorn",
    "publish": "2019-03-28T09:00:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
  <p>
    The funny thing about logical fallacies is that once you know them, you see them everywhere, but you still can't talk about them.
  </p>
  <p>Your company probably has great culture, right?</p>
  <p>
    You encourage transparency. Openness. Honesty and frankness above politics.
    <br />
    Right?
  </p>
  <p>
    Try telling someone their statements are fundamentally illogical and stem from bad reasoning and anti-intellectual thought.
  </p>
</section>
<section>
  <h2>Conventions Are Signifiers in Common Culture</h2>
  <p>
    I'm going to crib a bit from Don Norman's fantastic (if demoralizingly dense) <a href="https://smile.amazon.com/Design-Everyday-Things-Revised-Expanded-ebook/dp/B00E257T6C">The Design of Everyday Things</a> here; it is a phenomenal read and - after 31 years and only<sup><a href="#only">*</a></sup> one revision - it is still <strong>the</strong> preeminent book on the psychology of design (read: humans in the world).
  </p>
  <p>
    A signifier is something that tells where an action takes place. A door indicates that it provides access to another, separated area, the knob on the door is the signifier that tells you where the action to open the door takes place.
  </p>
  <p>
    The doorknob is a good example of a convention. Our entire culture (indeed, many cultures around the world), have been using a doorknob for a very long time. We all learn about doorknobs generally at a very young age, and that convention is now the standard.
  </p>
</section>
<section>
  <h2>Conventions Exist to Eliminate Mental Friction</h2>
  <p>
    Let me pose a question to you, reader. It is not a trick question, even though it seems like the answer must not be as obvious as it seems.
  </p>
  <p>
    Imagine that you would like to get information about your organization as configured in an application that you are logged into. That application provides a main menu navigation item called "Organization". Within the resulting page, there is a sub-navigation with - among others - these two navigation items in it:
  </p>
  <ul>
    <li>Users</li>
    <li>Account</li>
  </ul>
  <p>
    To find information out about your organization you choose:
  </p>
  <div style="display: block;">
    <label>
      <input type="radio" name="menu" />
      Users
    </label>
    <br />
    <label>
      <input type="radio" name="menu" />
      Account
    </label>
  </div>
  <p>
    If you chose <code>Account</code>, you are absolutely correct. The <code>Organization > Account</code> navigation heirarchy is where you would go to get information about your organization in this application.
  </p>
  <p>
    It's important to note at this juncture that you and I didn't make this up just now. We're using something called <em>convention</em>. I'm not teaching you a new word, you know it, and I know you know it. You know that you get organization information from <code>Organization > Account</code> the same way that you know how a doorknob works: it's a standard convention that you - and everyone else - learned a long time ago.
  </p>
  <p>
    Sometimes it makes sense to break convention. Usually these times are when you have lots of data that your new way is much better, and the benefits outweigh the costs. It never makes sense to install a set of hinges on a door and claim they're the handle.
  </p>
  <p>
    We rely on conventions to avoid having to constantly relearn new design languages.
  </p>
  <blockquote>
    Well in <strong>OUR</strong> house, you open the door by prying on this second set of hinges!
  </blockquote>
  <p>
    No.
    <br />
    <br />
    No. You are an absolute madman, and your decision is dumb.
  </p>
</section>
<section>
  <h2>Breaking Things Isn't a Personality</h2>
  <p>
    Imagine the above quiz where I asked what navigation you would use to find organization information. Now, imagine the answer was the opposite: to find some organization information, you do not go to the <code>Account</code> navigation item (it's not there!), you have to go to the <code>Users</code> navigation item.
  </p>
  <p>
    I think that, like me, your response would be something like:
  </p>
    <blockquote>
      Ah yes the users page
      <br />
      For all your company info needs
    </blockquote>
  <p>
    If the response to your disbelief is like the one that I was given:
  </p>
    <blockquote>
      Different products have different metaphors and are solving different problems
    </blockquote>
  <p>
    ...look out!
  </p>
  <p>
    This has four serious problems:
  </p>
  <ol>
    <li>It's a meaningless statement - a vacuous truth (an informal fallacy)</li>
    <li>It's a non sequitor argument (a formal fallacy)</li>
    <li>It's an example of special pleading (another informal fallacy)</li>
    <li>It conflates metaphoric commutation with egregious violations of convention</li>
  </ol>
</section>
<section>
  <h2>Meaningless Statements Are Not Deep Statements</h2>
  <p>
    If the wheels on my car only turned 5° and I told the manufacturer about it, and their response was:
  </p>
    <blockquote>
      Different vehicle wheels have different steering degrees and turn to solve different problems.
    </blockquote>
  <p>
    ...again: no.
  </p>
  <p>
    This is exactly as useful as the manufacturer replying that "some cars are red and some people like red cars." It is simultaneously true and utterly meaningless in this context.
  </p>
  <p>
    This brings up an important point: just because something is <strong>true</strong> does not make it insightful, useful, or applicable. This is <strong>yet another</strong> informal logical fallacy: the irrelevant conclusion.
  </p>
  <p>
    Your evidence may be persuasive, perhaps even objectively correct, but if it's not relevant to the larger point you are - and I quote another name for the irrelevant conclusion fallacy - <strong>missing the point</strong>.
  </p>
</section>
<section>
  <h2>Irrelevant Things Don't Matter</h2>
  <p>
    A <em>non sequitor</em> fallacy is a formal fallacy. In fact, it is <strong>the</strong> formal fallacy. It literally means "it does not follow." One specific type of <em>non sequitor</em> argument is "Affirming the Consequent."
  </p>
  <p>
    Affirming the consequent takes this form:
  </p>
  <ol>
    <li>If A, then B.</li>
    <li>B is true.</li>
    <li>Therefore, A is true</li>
  </ol>
   <p>
     A may in fact be true in this case, but the evidence ("B is true.") is not sufficient to show that A is true.
     <br />
     Restated with our navigation question:
  </p>
  <ol>
    <li>Our application should put organization information in the <code>Users</code> page because products have different metaphors and solve different problems.</li>
    <li>Products have different metaphors and solve different problems.</li>
    <li>Therefore, our application should put organization information in the <code>Users</code> page.</li>
  </ol>
  <p>
    Even <strong>if</strong> the consequent wasn't meaningless (see above section), the explanation given does not magically provide evidence for the antecedent.
  </p>
  <p>
    Of course, this formal fallacy only applies if the explanation was given as a good faith explanation for the illogical placement of the organizational information. If the statement was not intended as a formal explanation for why a thing is the way it is, a formal fallacy cannot - by definition - apply. I have to assume that arguments like this <strong>are</strong> given in good faith as a legitimate attempt at persuasion, but there's always a chance they may not be.
  </p>
  <p>
    That's alright, though, because...
  </p>
</section>
<section>
  <h2>Your Company Isn't Special</h2>
  <p>
    Special pleading is a kind of informal logical fallacy that attempts - with faulty or zero reasoning - to carve out an exception to a widely accepted principle.
  </p>
  <p>
    It essentially posits that, yes: these conventions exist for the world at large, but - for no reason - we are special and don't need to follow conventions.
  </p>
  <p>
    Take a look and this might be the fallacy that pops up the most often at your company.
  </p>
  <p>
    <strong>You</strong> can make accurate estimates, unlike everyone else.
    <br />
    <strong>You</strong> can use modals (These are popups. Modals are popups. Say it with me: a modal is a popup.) because you'll be responsible, unlike everyone else.
    <br />
    <strong>You</strong> can put organization information in the <code>Users</code> page because <strong>your</strong> products have different metaphors and solve different problems.
  </p>
  <p>
    Guess what? Your company is not "disrupting" the "where-are-my-settings" arena. You're not the AirBnB of settings pages.
  </p>
  <p>
    Speaking of "settings"...
  </p>
</section>
<section>
  <h2>Babbling Jibberish Isn't a Metaphor For Something, It's Just Jibberish</h2>
  <p>
    A metaphor is a way to reference something without actually talking directly about that thing.
  </p>
  <p>
    Here's a metaphor: <em>The world is a stage.</em>
    <br />
    You see, the world is not actually a stage, but now we can talk about how people in the world are just playing parts (actors on a stage!) and how people are born and die, and how they come into your life and leave your life (entering and exiting the stage!).
    <br />
    You see, we talk about the stage <strong>as if</strong> it were the world, because we've<sup><a href="#shakespeare">†</a></sup> set up an apt metaphor that is roughly commutative.
  </p>
  <p>
    A metaphor for organization information is "Account". It is "Settings". It is even "Profile" or "About".
  </p>
  <p>
    "Users" is not a metaphor for organization information, and it never will be. You cannot hand-wave away nonsensical violations of very normal conventions by positing something untrue.
    <br />
    Just this mere point alone is enough to make the entire statement meaningless: Yes, products use different metaphors, but that's relevant to neither the specific matter at hand nor the general topic at hand. Metaphors have nothing to do with nonsensical application heirarchy.
  </p>
</section>
<section>
  <h2>Are You Not <s>Entertained</s> Exhausted?</h2>
  <p>
    This is exhausting, right?
    <br />
    No, it's okay, you're exhausted by <strong>me</strong>, by <strong>this post</strong>. I get it. Remember up top? When I said "you still can't talk about them." You think I want to be <strong>this</strong> fucking asshole? You think I want to be the guy who quotes at least 4 different logical fallacies in one compound sentence?
    <br />
    This shit exhausts me. I'm exhausted by being unable to have actual discussions about actual things because every foregone conclusion is riddled with logical fallacies and outright untruths.
  </p>
  <p>
    I titled this post "Your Company is Not a Special Snowflake Unicorn" because I think this kind of behavior is rampant at SV-funded, YC-grad-type companies. "Startup" is a euphemism for "savior."
  </p>
  <p>
    Really this post could just be titled <strong>"I'm So Very Tired."</strong>
  </p>
</section>
<hr id="only" />
* I fibbed a bit. Yes, The Design of Everyday Things was only revised once, but Norman himself says that he "rewrote everything."
<hr id="shakespeare" />
† Okay, Billy Shakespeare came up with the metaphor.
