---{
	"id": "c6fbf3c2-b421-457b-9cda-fc7bee240d5f",
    "authors": [
        "trandolph"
    ],
    "title": "Authentication on the JAMstack",
    "publish": "2019-12-03T21:00:00-07:00",
    "status": "hidden",
    "type": "long",
    "rssExclusive": false
}---
<section>
		<p>
			As the JAMstack picks up steam across the industry, many developers used to out-of-the-box authentication baked into a single monolith or run by a backend framework are unsure of how to run a JAMstack site with more than one user (themselves!).
		</p>
		<p>
			Part of the power of the JAMstack is decoupling parts of an application from each other. Where your user authentication may have been inextricably linked to your framework du jour in the past, now - leveraging the power of APIs - an application developer can isolate authentication and authorization from the rest of the application architecture.
		</p>
		<p>
			In this first entry in my Authentication on the JAMstack series, we'll outline an overview of how to accomplish authentication, and we'll talk about each part in the abstract.
		</p>
		<p>
			In later parts, we'll cover each technical piece in detail.
		</p>
		<h2>Series Table of Contents</h2>
		<ol>
			<li>
			  <a href="/post/authentication-on-the-jamstack.html">Authentication on the JAMstack</a>
		  </li>
		  <li>
			  <a href="/post/the-database-and-how-it-s-the-most-important-part.html">The Database, and How It's The Most Important Part</a>
		  </li>
			<li>
				<a href="">Behind Every Good Serverless API is a Server Running the Code</a>
			</li>
			<li>
				<a href="">GraphQL is Better</a>
			</li>
			<li>
				<a href="">Logging In is Dumb if There's Nothing to Log In To</a>
			</li>
		</ol>
		<p>
			In this post (that's the first one, above), we'll go over the basic premise, talk broadly about authentication and touch on some high-level goals.
		</p>
		<p>
			In the second post, we'll architect a great database to store authentication information, and we'll talk a bit about how databases eventually influence every aspect of an application. Because of this, we'll focus a lot on normalization and getting the relational structure right immediately. It's very difficult to correct bad data structure later! Once we've defined our database, we'll also build the tooling to construct it.
		</p>
		<p>
			In the third post, we'll actually write the code to create new users, log users in, and log users out. We'll make sure we can exercise the code and perform all the actions from the command line.
		</p>
		<p>
			In the fourth post, we'll create an API with GraphQL that exposes our lovely authentication application. We'll do the same exercises that we had previously been doing from the command line, but now we'll do them over HTTP via our new GraphQL endpoint.
		</p>
		<p>
			Finally, in the fifth post, we'll build a very simple frontend application that will perform user registration, login, and logout. We'll also update our database to have exactly one, single piece of non-authentication data so that we can use it in our new UI to demonstrate how you can use JAMstack concepts to change what's displayed for authenticated users versus non-authenticated users.
		</p>
		<h2>Some Caveats About This Series</h2>
		<p>
			Before we do anything, I should note that I am neither a security expert nor a scalability engineer.
			<br />
			I don't believe there are any incredibly bad ideas encapsulated in this series, but it's worth noting that it could scale poorly as is.
		</p>
		<p>
			Keep in mind that just because something <em>could</em> scale poorly doesn't mean it <em>will</em>, it doesn't mean you'll ever hit the scale where it might matter, and it doesn't mean it's bad.
		</p>
		<p>
			Donald Knuth once said that "premature optimization is the root of all evil."
			<br />
			Scaling is an endeavor best left to the time when you need to scale, not before.
			<br />
			By the time anything in this series is your scalability bottleneck, you'll have the expertise, income, and employees to handle it with ease.
		</p>
		<h2>Authentication?</h2>
		<p>
			Merriam-Webster defines authentication as...
		</p>
		<p>
			Just kidding. Can you imagine?
			<br />
			Now I'm going to paraphrase <a href="https://en.wikipedia.org/wiki/Authentication">Wikipedia</a>.
		</p>
		<p>
			Authentication is essentially just proving some assertion. In the case of software, especially web applications, a visitor to the site asserts that they are a user that you know already, and the authentication is to prove that they actually are that user.
			<br />
			The simplest way to do this is with some identifier and a passphrase of some kind. The user provides both when they sign up ("this is who I am, and this is my secret!"). Every time they return, they provide the same values and you compare the originals to confirm who they are. This has a lot of problems, but it is the most basic form of authentication.
		</p>
		<p>
			In this series, we'll be building an authentication system that uses a similar basic premise, but layers a non-trivial amount of protections on top.
		</p>
		<h2>The Parts of an Authentication System</h2>
		<p>
			To do all this stuff, we'll need a minimum of four broad layers:
		</p>
		<ol>
			<li>a database (part 2)</li>
			<li>some way to efficiently get access to the database (also part 2)</li>
			<li>a way to perform our "business" logic: the stuff that actually creates users, logs them in, and logs them out (part 3)</li>
			<li>a layer of code to expose our sparkly new system to the internet (part 4)</li>
		</ol>
		<p>
			Beyond those four items, we'll also build a tiny frontend to actually use the system we built. It's no fun to build an API without seeing the fruits of that labor. We'll also use the frontend to demonstrate how a "static" (read: not actually static) JAMstack site can work for multiple users.
		</p>
		<h2>Series Goals</h2>
		<ol>
			<li>The primary goal of this series is to demonstrate all the parts that it takes to build an authentication system exposed only over an API (that is, not baked into anything that's rendering your views).</li>
			<li>In addition, the secondary goal of this series is to demonstrate that JAMstack sites can run off of statically compiled HTML and JavaScript files and still work (and be secure!) for a multitude of users.				</li>
		</ol>
		<h2>Can I Put This In Production?</h2>
		<p>
			I can't stop you from doing <strong>anything</strong>.
		</p>
		<p>
			At it's most fundamental (e.g. the concepts, the critical code), everything we'll cover in this series should absolutely be production-ready. I've seen much worse running at real companies. At a high level, we'll probably be taking a few shortcuts here and there, and the local server we're going to use is super, super bare bones, you probably shouldn't run a real production environment with it.
		</p>
		<h2>Prepare Yourself</h2>
		<p>
			Before you move on to part 2 of this series, you should ensure that you have <code>node</code> and <code>npm</code> installed on your computer. We'll be relying on features released in <code>node 13.2.0</code>, so you should ensure you have at least that version (I like to stay on the latest version on my personal computer).
		</p>
		<p>
			<strong>Are you ready?</strong>
		</p>
		<p>
			<strong>Let's build authentication on the JAMstack.</strong>
		</p>
		<p>
			<a href="/post/the-database-and-how-it-s-the-most-important-part.html">Next in the series: The Database, and How It's The Most Important Part</a>
		</p>
	</section>
