---{
	"id": "abc8ca19-7977-4cdc-9034-093cb3e3d08b",
    "authors": [
        "trandolph"
    ],
    "title": "Protect Your Users with HMAC Login Authentication",
    "publish": "2020-03-12T19:45:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
				<h2>Logins Send Passwords</h2>
				<p>
					<em>"Huuu durrr"</em> you say, <em>"that's the point of a login."</em>
				</p>
				<p>I disagree!</p>
				<p>
					The point of a login is to authenticate a person using the application. Sending their password to the backend is not necessarily part of that process.
				</p>
				<p>
					There are essentially four vulnerable parts of an authentication system:
				</p>
				<ol>
					<li>Creating the account (all the sensitive data can be intercepted)</li>
					<li>Storing the credentials (they can be stolen)</li>
					<li>Logging in (it can be replayed or stolen by a <em>man-in-the-middle</em>)</li>
					<li>The human (they can re-use their password other places, etc.)</li>
				</ol>
				<p>
					Of these, we can do pretty well in protecting against &num;2 and &num;3.
					We can't do much against &num;1 without drastically changing the typical sign-in flow.
					We also can't do much about most of the human factors that are out there, but we can protect our users against their password being stolen from <strong>us</strong> and <a href="https://en.wikipedia.org/wiki/Credential_stuffing">stuffed</a> into other websites, if they re-use passwords.
				</p>
				<p>
					Of course, ideally, our database would never be stolen and our users would never have their networks compromised (leading to their credentials being stolen). "Ideally" is the wrong way to build an authentication system, though, so we're going to look at a way to deal with the non-ideal.
				</p>
				<p>
					Granted, we will be using the most common method of sign-in (username plus password) and the most common method of storage (username plus hash). This is intentional.
					<br />
					We can get significant security gains by changing some of the magic between the user entering their information and the API authenticating them without making significant architectural changes.
				</p>
				<p>
					Of course, you <strong>can</strong> make significant architectural changes for even better security, but that's a topic for another time.
				</p>
			</section>
			<section>
				<h2>HMAC is Better</h2>
				<p>
					<em>"But how?"</em>
				</p>
				<p>
					Thanks for asking.
				</p>
				<p>
					<strong>HMAC</strong> stands for <em>Hash-based Message Authentication Code</em>.
				</p>
				<p>
					The basic premise is this: If I (the API) know a secret key that the user can continually regenerate using a secret passphrase, then we can both hash the same message and come up with the same result.
					<br />
					If we both come up with the same result without sharing the secret key (after the first time, more on that later), then I (the API) can theoretically assume that the user is the same person who originally created the account, because they've successfully generated the same secret key.
				</p>
				<p>
					Once more, this time with (paid) actors.
				</p>
				<p>
					Alice created an account on our website.
					<br />
					Our API - Bianca - has the secret key from when Alice created her account: <code>EGG</code>.
					<br />
					Alice knows the secret passphrase that will recreate the secret key: <code>PAN</code>.
					<br />
					When Alice visits the login form, she enters her username and her secret passphrase: <code>PAN</code>.
					<br />
					In her browser, our application uses a hash to recreate <code>EGG</code>. It then uses <code>EGG</code> to hash some data, like her username, into a third secret: <code>SCRAMBLE</code>.
					<br />
					The browser then sends Bianca the two pieces of data: the username and <code>SCRAMBLE</code>.
					<br />
					Bianca knows the same secret key already, so she does the same computation: <code>EGG</code> plus Alice's username results in <code>SCRAMBLE</code>. Since Bianca has come up with the same result as Alice asserted, Bianca assumes that Alice is who she says she is, and logs Alice in.
				</p>
				<p>
					For clarity (and the more visual among us), here's a sequence diagram that might help.
					<div class="mermaid">
						sequenceDiagram
							autonumber

							participant FC as Frontend Crypto
							participant A as Alice
							participant B as Bianca
							participant BC as Backend Crypto

							Note over A: Alice enters her<br />username (alice)<br />and passphrase<br />(PAN) into the site<br />login form.

							Note over A: Alice clicks the<br />Login submit<br />button

							rect rgba( 0, 0, 255, 0.3 )
								A ->> FC: "alice", "PAN"
								Note over FC: hash(<br />&nbsp;"alice" + "PAN"<br />) = "EGG"
								FC ->> A: "EGG"

								rect rgba( 255, 0, 0, 0.25 )
									A ->> FC: "EGG", "alice"
									Note over FC: hmac(<br />&nbsp;"EGG", "alice"<br />) = "SCRAMBLE"
									FC ->> A: "SCRAMBLE"
								end
							end

							rect rgba( 0, 255, 0, 0.25 )
								Note over A: message = {<br />&nbsp;"username": "alice",<br />&nbsp;"hmac": "SCRAMBLE"<br />}
								A -->> B: message
							end

							Note over A,B: Alice's secret passphrase, "PAN"<br />never goes across the network!

            	Note over B: Bianca already<br />knows the secret:<br />"EGG" from when<br />Alice signed up...

							rect rgba( 0, 0, 255, 0.3 )
								rect rgba( 255, 0, 0, 0.25 )
									B ->> BC: "EGG", "alice"
									Note over BC: hmac(<br />&nbsp;"EGG", "alice"<br />) = "SCRAMBLE"
									BC ->> B: "SCRAMBLE"
								end
							end

							Note over B: hmac "SCRAMBLE"<br />equals hmac in<br />message, therefore<br />username ("alice")<br />in message is<br />authenticated

							B -->> A: "alice" authenticated!
					</div>
				</p>
				<p>
					So the four parts of HMAC:
				</p>
				<ul>
					<li><strong>Hash-based</strong>: Alice and Bianca both perform a hash of some data (numbers 3 & 6 in the diagram)</li>
					<li><strong>Message</strong>: Alice (via our login form) sends a message asserting who she is to Bianca (number 5 in the diagram)</li>
					<li><strong>Authentication</strong>: Since the passphrase and key are not transferred, Bianca believes the authenticity of Alice's message if both Alice and Bianca come up with the same hash (number 8 in the diagram)</li>
					<li><strong>Code</strong>: <code>SCRAMBLE</code> is the code both Alice and Bianca generate. (numbers 4 & 7 in the diagram)</li>
				</ul>
			</section>
			<section>
				<h2>Interceptions: 0</h2>
				<p>
					Note that <strong>only</strong> Alice knows the secret passphrase <code>PAN</code>. It never leaves her browser.
					<br />
					This is how we protect Alice from having her password stolen by <a href="https://en.wikipedia.org/wiki/Man-in-the-middle_attack"><em>man-in-the-middle</em></a> attacks.
					Even if she's on a compromised network, Alice's password never leaves her browser.
					If we wanted the additional security of a <a href="https://en.wikipedia.org/wiki/Salt_(cryptography)">salt</a>, we could change our signup and login flows to first submit the username, which would return the salt, and then the signup or login would proceed normally but using the salt to enhance the entropy of whatever passphrase is used.
				</p>
				<p>
					We want to leave the login flow alone as much as possible, though, so we'll use Alice's username as her salt.
					It's not a perfect salt, but it's better than nothing.
					If Alice's password is <code>password</code> (shame on you, Alice), and her username is <code>alice1987</code>, at least our combined secret passphrase will be <code>alice1987password</code> (or <code>passwordalice1987</code>) instead of just <code>password</code>. It's an improvement, even if minimal.
				</p>
			</section>
			<section>
				<h2>Message Authentication 😍</h2>
				<p>
					We have one more thing we can do to protect Alice.
				</p>
				<p>
					If someone is snooping on Alice's network while she's logging in - maybe from unsecured coffee shop wifi - they could grab her login request (remember, it's <code>alice1987</code> and the authentication code <code>SCRAMBLE</code>).
					<br />
					Once someone has that info they could do anything with it. They could try to reverse the hash algorithm to figure out her secret passphrase, or they could re-use the same request to make Bianca think that Alice is trying to log in again.
				</p>
				<p>
					The only thing we can do about the former is regularly audit our hash algorithms to make sure we're using best-in-class options so that figuring out the secret phrase will take longer than it's worth.
					<br />
					We can - however - protect against the latter attack, called a <a href="https://en.wikipedia.org/wiki/Replay_attack">replay attack</a>.
				</p>
				<p>
					To protect against replay attacks, all we have to do is add a timestamp to the message!
					When Bianca checks the authentication request, she'll also check that the timestamp isn't too old - maybe a maximum of 30 seconds ago.
					If the request is too old, we'll consider that a replay attack and just deny the login.
				</p>
				<p>
					The beauty of message authentication is that as long as our front end sends all of the information <strong>and</strong> it includes that information in the HMAC (<code>SCRAMBLE</code>), the API (Bianca) can always validate the information.
				</p>
			</section>
			<section>
				<h2>Show Me How Now, Brown Cow</h2>
				<p>
					The amazing thing about this is it just uses off-the-shelf tools baked into virtually every programming language.
					Even web browsers provide (some of) these tools by default as far back as IE11!
					<br />
					Sadly, one crucial piece - the TextEncoder - is not supported in Internet Explorer at all, so if you need to support IE, some polyfilling is necessary.
					Fortunately, everything here is polyfillable!
				</p>
				<p>
					To get us started, we'll build a couple of helper functions to do basic cryptographic work.
				</p>
				<pre><code>function hex( buffer ){
	var hexCodes = [];
	var view = new DataView( buffer );

	for( let i = 0; i < view.byteLength; i += 4 ){
		hexCodes.push( `00000000${view.getUint32( i ).toString( 16 )}`.slice( -8 ) );
	}

	return hexCodes.join( "" );
}

export function hash( str, algo = "SHA-512" ){
	var buffer = new TextEncoder( "utf-8" ).encode( str );

	return crypto
		.subtle
		.digest( algo, buffer )
		.then( hex );
}</code></pre>
				<p>
					These two functions take a stream of data and convert it to hexadecimal (<code>hex</code>) and take a string and a hashing algorithm to create a hash (<code>hash</code>).
					<br />
					Note that <a href="https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto">SubtleCrypto</a> only supports a small subset of the possible algorithms for digesting strings.
					For our use-case, however, SHA-512 is great, so we'll default to that.
				</p>
				<p>
					Note that <code>hex</code> is synchronous, but <code>hash</code> returns a Promise that eventually resolves to the hexadecimal value.
				</p>
				<p>
					Next up, we need a way to generate a special key that will be used to create the authentication code.
				</p>
				<pre><code>function makeKey( secret, algo = "SHA-512", usages = [ "sign" ] ){
	var buffer = new TextEncoder( "utf-8" ).encode( secret );

	return crypto
		.subtle
		.importKey(
			"raw",
			buffer,
			{
				"name": "HMAC",
				"hash": { "name": algo }
			},
			false,
			usages
		);
}</code></pre>
				<p>
					Here, <code>makeKey</code> takes Alice's secret passphrase, some algorithm (SHA-512 again, in our case), and a list of ways the key is allowed to be used.
					<br />
					For our intent, we only need to be able to sign messages with this key, so our <code>usages</code> can just stay <code>[ "sign" ]</code>.
				</p>
				<p>
					What we get back is a Promise that eventually resolves to a <a href="https://developer.mozilla.org/en-US/docs/Web/API/CryptoKey">CryptoKey</a>.
				</p>
				<p>
					Finally we need a way to generate signed HMAC messages.
				</p>
				<pre><code>export async function hmac( secret, message, algo = "SHA-512" ){
	var buffer = new TextEncoder( "utf-8" ).encode( message );
	var key = await makeKey( secret, algo );

	return crypto
		.subtle
		.sign(
			"HMAC",
			key,
			buffer
		)
		.then( hex );
}</code></pre>
				<p>
					Here, <code>hmac</code> takes Alice's secret passphrase, and our message that we want to authenticate (plus our hash algorithm).
					<br />
					What we get back is a Promise that eventually resolves to a long hexadecimal string.
					That hex string is the authentication code!
				</p>
				<p>
					When our API (Bianca) and our front end agree on the mechanism for creating messages to be authenticated, as long as the result from <code>hmac</code> on both sides agrees, the message has been authenticated!
				</p>
			</section>
			<section>
				<h2>Sign Up & Log In</h2>
				<p>
					So let's say all that code above is off in a file called <code>Crypto.js</code>.
					<br />
					Here's a file with two functions in it that create a new account for a user, and log a user in.
				</p>
				<pre><code>import { hash, hmac } from "./Crypto.js";

async function createAccount( username, passphrase ){
	var secret = await hash( `${username}${passphrase}` );

	return fetch( "/signup", {
		"method": "POST",
		"body": JSON.stringify( {
			secret,
			username
		} );
	} )
}

async function login( username, passphrase ){
	var now = new Date();
	var secret = await hash( `${username}${passphrase}` );
	var authenticationCode = await hmac( secret, `${username}${now.valueOf()}` );

	return fetch( "/login", {
		"method": "POST",
		"body": JSON.stringify( {
			"hmac": authenticationCode,
			"timestamp": now.toISOString(),
			username
		} )
	} )
}</code></pre>
				<p>
					Here's a sequence diagram for signing up.
					<div class="mermaid">
						sequenceDiagram
							autonumber

							participant FC as Frontend Crypto
							participant A as Alice
							participant B as Bianca

							Note over A: Alice enters her<br />username (alice)<br />and passphrase<br />(PAN) into the site<br />create account<br />form.

							Note over A: Alice clicks the<br />Account Create<br />button

							rect rgba( 0, 0, 255, 0.3 )
								A ->> FC: "alice", "PAN"
								Note over FC: hash(<br />&nbsp;"alice" + "PAN"<br />) = "EGG"
								FC ->> A: "EGG"
							end

							rect rgba( 0, 255, 0, 0.25 )
								Note over A: signup = {<br />&nbsp;"username": "alice",<br />&nbsp;"secret": "EGG"<br />}
								A -->> B: signup
							end

							Note over B: Bianca stores "EGG"<br />in the table<br />"authentications"<br />for the username<br />"alice"

							B -->> A: "alice" account created!
					</div>
					The sequence diagram for logging in is identical to the one at the beginning of this post.
				</p>
			</section>
			<section>
				<h2>What About The API?</h2>
				<p>
					Good question.
					<br />
					It does the exact same work you've always done, just using HMAC now.
				</p>
				<p>
					In the case of account creation, nothing has changed.
					It just so happens the <code>secret</code> we receive from the user is a bit longer (and more random) than usual.
					Store it safely in your authentications database table associated to the username.
				</p>
				<p>
					For logins, your API will be doing a bit more work.
					<br />
          Here's a bit of rough psuedo-code (<strong>unlikely to run, never tested</strong>) for a <code>node</code> login back end.
				</p>
				<pre class="discourage-copy"><code>import { createHmac } from "crypto";

function login( request, response ){
	var now = Date.now();
	var oldest = now - 30000;
	// fromISO from your favorite DateTime library
	var timestamp = fromISO( request.body.timestamp ).valueOf();
	var { hmac, username } = request.body;
	// db is a Database abstraction layer here, use your favorite!
	var secretKey = db.Authentications.getSecretKeyForUser( username );

	var authenticator = createHmac( "sha512", secretKey );

	// Note the parity here with the front end, we are hashing the same message
	authenticator.update( `${username}${timestamp}` );

	// If the one we created matches the one from the front end, we're authenticated
	var isAuthenticated = authenticator.digest( "hex" ) == hmac;

	// Except.......

	if( oldest >= timestamp ){
		// HEY THIS LOOKS LIKE A REPLAY ATTACK!
	}
	else{
		return isAuthenticated;
	}
}</code></pre>
			</section>
			<section>
				<h2>I Zoned Out When I Saw <code>crypto</code>, How Is This Better?</h2>
				<p>
					Thanks for asking.
					<br />
					This is better than the standard authentication process for a number of reasons:
				</p>
				<ul>
					<li>
						<p>
							<strong>Better protection against password reuse.</strong>
							<br />
							You can't protect your user from other websites stealing their password and stuffing it into your website (other than disallowing known-breached passwords, a topic for another time), but you can add a layer of protection.
							<br />
							By only storing a hashed value based on Alice's secret passphrase, you reduce the potential of an attacker being able to steal your data and use it elsewhere.
							<br />
							This is effectively the exact same policy as never storing plaintext passwords, except we're doing the hashing on Alice's computer, which means...
						</p>
					</li>
					<li>
						<p>
							<strong>Better protection against <em>man-in-the-middle</em> attacks.</strong>
							<br />
							Granted, you should be running your entire website over SSL with <a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security">HSTS</a>, but just in case you're not, or just in case someone manages to perform a complicated <a href="https://en.wikipedia.org/wiki/Downgrade_attack">SSL downgrade attack</a> (only possible without HSTS!), HMAC further protects the all-important passphrase.
							<br />
							Since the only thing exiting Alice's web browser under most circumstances is the Authentication Code, it's a lot (years, maybe) of work to bruteforce that hash and extract the secret key.
							<br />
							Then, that key is only useful on your website, so it's even more work to bruteforce the key and extract the original passphrase.
							Generally speaking, this kind of effort is not worth it to the average website attacker.
							<br />
							If you run a website with government, banking, or sensitive personal secrets, I assume this is all old news to you and you're probably doing something even better... right?
						</p>
					</li>
					<li>
						<p>
							<strong>Better protection against replay attacks.</strong>
							<br />
							This is pretty straight-forward.
							Because we're mixing a timestamp into the message to be authenticated, an attacker has only one option to be able to pretend they're Alice after intercepting her login request: bruteforce the hash to extract the secret key, then update the timestamp and regenerate the message authentication code.
							<br />
							Again, this is generally more effort than it's worth.
						</p>
					</li>
				</ul>
			</section>
			<section>
				<h2>Is It All Rainbows?</h2>
				<p>
					Of course not.
					<br />
					Without a doubt, this is <strong>better</strong> than the standard login form that sends a username and password to an API.
					<br />
					However, it does still have some drawbacks.
				</p>
				<ul>
					<li>
						Like almost every authentication system, it relies on strong cryptographic hashing.
						<br />
						Because it uses lots of hashes, it's a bit more secure because there's more bruteforcing to do, but hashes are fundamentally based on the idea that it takes a really long time to break them, not on the idea that they're infallible.
					</li>
          <li>
            On that note, the hashing and key derivation here is computationally expensive.
            <br />
            Of course, this is inherent in the idea of using hashes; they are fundamentally intended to be expensive because if they were cheap and fast they would be easy to break. However, for users that are underprivileged (like in developing countries, for example) or otherwise running under-powered hardware, HMAC may cause non-trivial delays during login.
            <br />
            For modern, full-powered hardware, it's likely your login flow will slow down by a few milliseconds, if it slows down noticeably at all. For under-powered devices, your users could see drastic slowdowns into the one second or worse range.
            <br />
            Above all: know your audience. If your audience is entirely on under-powered devices, the resulting slowdown during login could be a deal-breaker. However, keep in mind that there is always a trade off between convenience and security. Perhaps the non-trivial processing time for your users will eventually be balanced out by your data being particularly secure if you ever get breached.
          </li>
					<li>
						The secret is still sent across the internet.
						<br />
						Once Alice's computer has hashed together her username and passphrase during signup it takes the secret key and... sends it across the internet.
						<br />
						This is the only time that happens, but it only takes one snooping <em>man-in-the-middle</em> to ruin everything.
						There are ways to get around this problem, but they involve a very different signup architecture.
						<br />
						In an effort to make this as usable as possible for people with boring old username/password signup flows, this will have to do; we need to get that secret key to the back end somehow.
					</li>
					<li>
						There's nothing you can do about the human element without significantly changing your signup and login flow.
						<br />
						Guess how hard it would be for an attacker to guess a username <code>bill1968</code> and password <code>billsmith1968</code>.
						<br />
						It's likely that you should be designing systems that detect and ban or discourage this kind of flawed input on signup, but ultimately humans are the weakest part of any security system.
					</li>
				</ul>
			</section>
			<section>
				<h2>So Should I Do It or Nah?</h2>
				<p>
					Yeah, you should do it.
				</p>
				<p>
					It takes maybe an hour to set this stuff up, and the security gains are worth far, far more than an hour of your time.
				</p>
				<h3>Happy Hashing!</h3>
        <br />
			</section>
			<section>
				<h2>Thanks!</h4>
				<p>
					This post could not be what it is without help from the following folks:
				</p>
				<ul>
          <li><a href="https://www.linkedin.com/in/ilgraves/">Ian Graves</a>: For explaining HMAC in a way that finally clicked for me so that I felt like I could go learn how to do it in a browser.
					<li><a href="https://twitter.com/_davejs">David Leger</a>: For reviewing an early version of this post (that will probably never see the light of day) and reminding me to keep it simple.</li>
					<li><a href="https://gitlab.com/tmccaslin">Taylor McCaslin</a>: For reminding me not to make it easy to copy/paste untested code, and for lots of insight into the complexities of client-side hashing and key derivation performance.</li>
					<li><a href="https://gitlab.com/cmoberly">Chris Moberly</a>: For a thorough review and for reminding me to include a note about HSTS and HTTPS/SSL.</li>
					<li><a href="https://gitlab.com/jeremymatos">Jeremy Matos</a>: For reminding me that visual things are easier to understand, and prompting me to include the sequence diagrams.</li>
				</ul>
			</section>
			<script src="https://unpkg.com/mermaid@8.4.8/dist/mermaid.min.js"></script>
  <script>
	  mermaid.initialize({
        "startOnLoad": true
    });

  var scale = function scale( size ){
		var scales = {
			"200": 1.3,
			"400": 1.2,
			"600": 1.1,
			"10000": 1
		};
		var keys = Object.keys( scales );

		return Object.values( scales ).find( ( _, i ) => size <= keys[ i ] ) || 1;
	}

	setTimeout( () => {
		document
			.querySelectorAll( ".mermaid .note" )
			.forEach( ( note ) => {
				let w = parseInt( note.getAttribute( "width" ), 10 );
				let x = parseInt( note.getAttribute( "x" ), 10 );
				let expanded = w * scale( w );
				let difference = expanded - w;
				let newX = Math.round( x - ( difference / 2 ) );
				let texts = note.parentNode.querySelectorAll( ".noteText,tspan" );

				note.setAttribute( "width", Math.round(expanded ) );
				note.setAttribute( "x", Math.round( x - ( difference / 2 ) ) );
				console.log( texts );
				texts.forEach( ( text ) => {
					let x = parseInt( text.getAttribute( "x" ), 10 );

					text.setAttribute( "x", Math.round( x - ( difference / 2 ) ) );
				} );
			} );
	}, 1500 );
  </script>
