---{
		"id": "15a22427-33e3-4061-9948-de5ca6921894",
    "authors": [
        "trandolph"
    ],
    "title": "The Web Application Architecture (long draft)",
    "publish": "2023-07-04T12:05:00-06:00",
    "status": "previewable",
    "type": "long",
    "rssExclusive": false
}---
<main>
My hope is that this isn't an incredibly <strong>long</strong> post, but - by its nature - it will be a fairly <strong>broad</strong> post, so here's what you can expect in the rest of this document:
<ol>
  <li>Introduction</li>
  <li>
    The Front End
    <ol>
      <li>General Structure</li>
      <li>(De-)Complexity</li>
      <li>Native Features</li>
    </ol>
  </li>
  <li>
    The Back End
    <ol>
      <li>Data at Rest: You need JSON</li>
      <li>The "A" in API does not stand for access</li>
    </ol>
  </li>
  <li>
    Network Communication
    <ol>
      <li>Websockets</li>
      <li>Structured Socket Message Protocol</li>
    </ol>
  </li>
  <li>
    Generalized Principles
    <ol>
      <li>Small</li>
      <li>Events-first</li>
      <li>Native code</li>
      <li>Single-purpose</li>
      <li>Encapsulated</li>
    </ol>
  </li>
</ol>
<section>
  <h2>Hello!</h2>
  <p>
    This is how web applications should be built.<sup>*</sup>
    <br />
    Sure, there are exceptions - but they are far less common than people would have you believe. Sure, there are situations where certain things cannot possibly work - but those situations are exceedingly rare. Much, much rarer than you're thinking.
  </p>
  <p>
    So, here's how web applications should be built, starting with...
  </p>
</section>
<section>
  <h2>The Front End</h2>
  <p>
    I have more opinions and experience with the front end than with the back end, so most of my focus is there. That said, I have done plenty of back end work, so we'll get to some notes about that later.
  </p>
  <h3>General Structure</h3>
  <h4>Pages</h4>
  <p>
    Your application should have pages.
    <br />
    Real, honest-to-goodness pages that live as files on a server. You don't have to have a ton, but you should probably have one for every high-level concept in your application. When I was demonstrating the power of <a href="https://www.npmjs.com/package/@thomasrandolph/taproot">taproot</a>, I outlined an application where you could view and rate dogs. In a simple application like this, you might have three pages: All dogs, a single dog, and your own profile page.
    <br />
    This is an <a href="https://gitlab.com/rockerest/taproot/-/blob/de3142cc405b9181379ed4f527340a39f554c23f/README.md#empa">"Enhanced Multi-Page Application" (EMPA)</a>. You don't need a new HTML file for every single variation, but you should have some!
  </p>
  <h4>Components</h4>
  <p>
    An application should be broken down into unique, reusable parts.
  </p>
  <h5>Unique</h5>
  <p>
    HTML already has a button element. Stop it.
    <br />
    If you think you can't use a <code>&lt;button></code> element successfully, consider instead why your design system is insufficient to define what your interface should look like, why the tools you're using prevent you from simply implementing that design language, or why you don't believe other engineers could possibly understand the standards around using the tools you have for free.
    <br />
    Expand this to every component you're considering building. What are the real, defined reasons that it needs to exist. Are you solving a real problem? Are you solving a problem that needs to be solved with more code?
  </p>
  <h5>Reusable</h5>
  <p>
    What other page are you going to use your UserProfileEditForm component on? Why - <em>exactly</em> does this need to exist?
  </p>
  <h4>Actions</h4>
  <p>
    If a page renders content and components, and a component provides user interface, an action defines what your application can do. It is <em>behavior</em>: wholly separate from interface and connectivity, actions define the compendium of abilities that your application provides. Curate them carefully, and tend them regularly. We'll talk more about these later.
  </p>
  <h4>Side Effects</h4>
  <p>
    Side effects are your application's uncontrollable appendages. Network connections are a staple here. Making, responding to, and handling failures of network requests are basic features of almost any web application. You could also lump in writing to the user's disk, accessing a device peripheral, or anything else that requires reaching beyond the boundaries of the application code itself.
    <br />
    These are separate from actions because they are - in some sense - even more specific abilities which can be reused by other actions, but also because they are distinct from software. Side effects depend on the real world: the internet works, a microphone stays connected, and so on. Interfacing with the real world requires special consideration.
  </p>
  <h3>(De-)Complexity</h3>
  <p>
    I beg of you, please watch <a href="https://www.infoq.com/presentations/Simple-Made-Easy/">this video of Rich Hickey giving a talk titled "Simple Made Easy"</a> at Strangeloop 2011.
  </p>
  <p>
    One of the many take-aways from that talk is this: Simple is the opposite of complex, and complex means "interconnected" or "intertwined." Simple code is not monolithic, because monoliths touch many parts of your application, inherently increasing the complexity of it.
  </p>
  <h4>Actions versus Interactions</h4>
  <p>
    Beyond pages, which simplify your application's entry point (by reducing its scope), the rest of your application should be broken down into actions and interactions.
  </p>
  <p>
    Many readers will feel as though their application is already broken down into actions and interactions. After all, what are Redux or Vuex actions if not actions separated from the UI component - the interaction?
    <br />
    <aside>
      Modern front end web development has designed a deeply misleading set of best practices. To begin with - and we will simply gloss over this for now - a global variable that holds all of your state in memory is not the only target for actions in your application. And in any case, the way these faux actions are recommended to be used is to simply re-import them back into your interface components. This is imperative programming but with more steps!
    </aside>
    <br />
  Separating the actions that modify this state out of your interactive user interface is a euphemism for real, considered separation of what your application can do, and how your users will interact with it.
  </p>
  <p>
    Fundamentally: <strong>actions are the things your application can do</strong>. Full stop.
    <br />
    Interactions are the ways you expose end users to your application's capabilities.
  </p>
  <p>
    Consider the rise of GraphQL and Apollo being dropped directly into individual interface components. This is a violation of the principles of good design: Never once in their life has a user thought "I'd like to trigger a GraphQL request when I click this button." The <em>mechanism</em> by which a user's need is met (and that need is "I'd like to see more of this paginated list of content.") is not - and should never be - a part of the interaction by which the user expresses their need.
    <br />
    One of the keys here is that applications may reuse actions to fulfill multiple purposes, but a user almost never uses the same interactions to express multiple needs. That is to say: Interactions are specific to a purpose, while actions may be generalized to a solution.
  </p>
  <h4>Disparate, yet cohesive</h4>
  <blockquote>
    Messages are sent to a recipient. Events are broadcast.
  </blockquote>
  <p>
    A pertinent question at this point might be: If my interface doesn't trigger network requests or modify the global state variable, how does anything work?
  </p>
  <p>
    Events, of course!
  </p>
  <p>
    The entire web - and indeed the web browser you're reading this in - is fundamentally based on messages. HTTP is a back-and-forth of mostly-stateless messages being passed back and forth. The fundamental nature of HTML and the browser DOM is based - deeply and inextricably - on events.
    <br />
    The difference between message-based and event-based systems is subtle, but crucial: Messages are sent to a recipient. Events are broadcast.
    <br />
    This delta between the two modes of software operation makes all the difference when we're talking about de-complexifying an application. Rather than <em>messaging</em> the store that something should update, the application signals that an event has occurred. <strong>If</strong> that event would require an update to the store, that can happen, but so can any number of other things.
  </p>
  <h5>High- versus low-complexity</h5>
  <p>
    In an application with high complexity, code becomes more and more specialized and ultimately highly fractured as the use cases for what used to be a general handler bifurcate over and over to accommodate new and subtly different expected outcomes. This high complexity comes from connecting the action directly to the interaction, because it's impossible to know what future actions may need to be attached to an interaction.
    <br />
    In an application with low complexity, an interaction is never attached to any action. Interactions only ever trigger events. It's a much more polite way of writing software. Instead of "Do this!" the code instead says "This happened, if you'd like to know!" Now, when actions need to occur as a result of that interaction, the complexity is strictly encapsulated outside the interaction. Critically, this means that when the action needs to bifurcate for some reason, that complexity can be managed (or mitigated!) in a single place, where the rest of the application can also benefit from it, and the interaction never changes.
  </p>
  <h3>Native Features</h3>
  <p>
    An application with low base potential for technical debt begins with native features wherever possible.
  </p>
  <p>
    You probably don't need your third party component library, because every browser supports Custom Elements.
  </p>
  <p>
    You probably don't need your third party CSS obfuscation or DOM management tooling, because every browser supports Shadow DOM.
  </p>
  <p>
    You probably don't need to worry about anything else with CSS, because every browser supports Cascade Layers.
  </p>
  <p>
    You may not need a page animation manager or overbearing DOM-clobbering tooling because you can use View Transitions (for most of your users).
  </p>
  <p>
    You don't need to create a giant global variable in RAM with a Flux tool (like Redux or Vuex) or with a GraphQL tool (like Apollo), because every browser supports IndexedDB.
  </p>
  <p>
    You certainly don't need an event emitter, or to re-use a component library as an event emitter, because every browser has always supported <code>Event</code> and <code>EventTarget</code>.
  </p>
</section>
<section>
  <h2>The Back End</h2>
  <h3>Data at Rest: you need JSON</h3>
  <h3>The "A" in API does not stand for access</h3>
</section>
<section>
  <h2>Network Communication</h2>
  <h3>Websockets</h3>
  <h3>Structured Socket Message Protocol</h3>
</section>
<section>
  <h2>Generalized Principles</h2>
  <h3>Small</h3>
  <h3>Events-first</h3>
  <h3>Native code</h3>
  <h3>Single-purpose</h3>
  <h3>Encapsulated</h3>
</section>
<hr />
<section>
  <sup>*</sup>: You might sense a tongue-in-cheek tone here, and while that's very perceptive of you, the reality is that I really do believe this extremely strongly, and you should - if you consider yourself a serious professional web developer - take this all very seriously.
</section>
