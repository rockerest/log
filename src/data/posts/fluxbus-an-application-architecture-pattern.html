---{
	"id": "94bf7262-2497-4584-84ba-ec1416ff793e",
    "authors": [
        "trandolph"
    ],
    "title": "Fluxbus - an application architecture pattern",
    "publish": "2019-01-06T21:05:00-07:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
	<h2>Facebook the Behemoth</h2>
	<p>
		Facebook is pretty good at a <a href="https://www.nytimes.com/2018/03/19/technology/facebook-cambridge-analytica-explained.html">couple</a> <a href="https://www.forbes.com/sites/alicegwalton/2012/04/05/the-true-costs-of-facebook-addiction-low-self-esteem-and-poor-body-image/">of</a> <a href="https://pitchfork.com/news/facebook-let-spotify-and-netflix-access-users-private-messages-report/">very</a> <a href="https://www.nytimes.com/2018/09/28/technology/facebook-hack-data-breach.html">interesting</a> <a href="https://www.forbes.com/sites/alicegwalton/2015/04/08/new-study-links-facebook-to-depression-but-now-we-actually-understand-why/">things</a>.
		<br />
		It also has <a href="https://www.statista.com/statistics/264810/number-of-monthly-active-facebook-users-worldwide/">two and a quarter <strong>billion</strong></a> monthly active users. The world has around 7.6 billion people in it, which means something like 30% of the world uses Facebook at least semi-regularly.
	</p>
	<p>
		Let's stipulate - for a moment - that you are not Facebook.
		<br />
		Let's stipulate - just for now - that you do not have 2.25 billion users. Or even 1 billion. Or even 1 million. If you have 1 million users: congratulations! You are 4.44e-4 the size of Facebook, or .000444 the size - 4.44 hundred-thousandths the size.
	</p>
	<figure>
		<img src="https://res.cloudinary.com/rockerest/image/upload/v1639130788/facebook-versus-1million-users.png" />
		<figcaption>The red block is you, with 1,000,000 active users!</figcaption>
	</figure>
	<p>
		Given that stipulation, can we agree that you do not have the same problems Facebook has? You have the same problems that Facebook has in the same way that - say - your household has the same budget problems that the U.S. Government has.
		<br />
		I have yet to write at any length about the negatives (and it is <strong>all</strong> negatives) of <a href="https://en.wikipedia.org/wiki/Cargo_cult_programming#Cargo_cult_software_engineering">cargo-cultism</a> (other than another <a href="https://log.rockerest.com/post/the-seven-things-that-make-a-senior-engineer.html">one-off comment</a>), but suffice to say: if you think <strong>anything</strong> a company like Facebook is doing solves the same problems you are having, you are woefully deluded - by your own doing.
	</p>
	<p>
		Now, this doesn't mean that technological accomplishments that a company like Facebook is doing can't be applied to everyone else (and we are really <em>everyone else</em>; Facebook is literally in a league of its own in terms of userbase).
		<br />
		It's just that the things Facebook is doing need to be tweaked.
		<br />
		Take - for example - the Flux architecture (and specifically it's outspringing: Redux). These things seem to look pretty good, but once you really start using them heavily, you begin to make all kinds of sacrifices because they <em>prescribe</em> an application structure.
		<br />
		For some, this may be fine. Indeed, I don't think many people will disagree that a pattern like Redux is prescriptive. <em>"Yes. That's kind of the point,"</em> they will say. Unfortunately, I have found that this level of prescriptivism doesn't work well for me.
	</p>
</section>
<section>
	<h2>Why Redux (and Flux, sort of) Doesn't Work For Me</h2>
	<ol>
		<li>
			<strong>Asynchronous is really complicated</strong>
			<p>
				Have you ever used Redux? If you have, then you probably encountered a huge problem <strong>almost immediately</strong>: You can't just load data any more.
				<br />
				Since everything has to result in a synchronous change to the store that subscribers then consume, your actions have to be thunks, and you probably need to use a complex middleware to interrupt the normal action processing pipeline until your async action finishes.
			</p>
			<p>
				This isn't strictly necessary with <strong>either</strong> generic Flux architectures or with Redux specifically, but if you implement asynchronous behaviors outside the "Redux way", well, you're not doing it the "Redux way".
				<br />
				All sorts of things start to get more complicated. And - after all - isn't the point to standardize the way your data flows in one direction?
			</p>
			<p>
				Because Flux is much more general, I think asynchrony fits much better into that pattern, but it's still not easy: there's a whole example project <a href="https://github.com/facebook/flux/tree/master/examples/flux-async">just for async</a>.
			</p>
		</li>
		<li>
			<strong>Every app I've ever worked on uses more than one type of storage</strong>
			<p>
				In most of my applications, there's a heirarchy of data. The absolute source of truth for 1 or two bits of information is a cookie (this cookie contains an authentication token, maybe one or two other things).
				<br />
				We need a cookie because we need login to work across multiple subdomains, and solutions that don't use cookies are a ridiculous re-invention of the top level domain cookie that's worked forever.
				<br />
				When the application(s) start, they check this cookie (what if the user logged in as a different user between the previous load and now?) and load the correct information from it into <code>localStorage</code>, sometimes triggering API requests to get more complete information and store <strong>that</strong> information in <code>localStorage</code>.
			</p>
			<p>
				During runtime, the application might change certain values in <code>localStorage</code> that need to persist to the next load, but don't need to go back to the server.
				<br />
				Even if the application doesn't do that, it absolutely will store a large amount of data in one (of many possible) <code>indexedDB</code> databases.
				<br />
				This data is indexed, timestamped, and keyed by request URL (if you request <code>a?b=1</code>, and we have that data, you get a free resolution, no network needed!).
			</p>
			<p>
				Finally, also during runtime, the application stores some information in the "window" storage also known as the global object in a custom namespace. This is just a scratch space for one-off memory, but it's very convenient for storing something complex like a function. Like a router.
			</p>
			<p>
				Flux (and Redux) give you exactly one kind of storage - the latter, a runtime-level storage. You can freeze it to a cold state and hydrate it again on app load, but this feels immensely wasteful.
				<br />
				Why is my data not just there to begin with? That's the place it lives, why am I storing it in the Flux store as a dumb middleman when I could just be using the <strong>actual</strong> storage directly?
			</p>
		</li>
		<li>
			<strong>Every single listener is called for every single change</strong>
			<p>
				This one is a shared problem in both Redux and Flux.
				<br />
				It's particularly bad in Redux, which demands that you use a single store for <em>everything</em>, but it's just scaled down to each Flux store.
			</p>
			<p>
				Since the subscription pattern in Redux is <code>store.subscribe( someCallback )</code>, there is no way to filter how many functions are run when the state updates.
				<br />
				Imagine a moderately complex application that has a state value like <code>{ messages: { loading: true } }</code> and a few hundred subscribers.
				<br />
				How many of those subscribers care that the messages stop loading at some point? Probably 1. Wherever your chat module is, it's probably the <strong>only</strong> place that cares when the messages stop loading and it can hide a spinner.
				<br />
				But in this subscription paradigm, all of your few hundred subscribers are called, and the logic is checked to determine if they need to update. The inefficiency is mind-boggling.
			</p>
			<p>
				Again, Flux doesn't quite have the exact same problem: not only because of the fact that you can have multiple stores for specific intents, but also because it doesn't prescribe any particular subscription pattern.
				<br />
				You <strong>could</strong> implement a pre-call filter that allows subscribers to only be called for particular actions that they care about, but this is so far off the beaten path it's never been seen by human eyes before.
			</p>
			<p>
				The alternative is to only <a href="https://github.com/facebook/flux/blob/master/examples/flux-todomvc/src/containers/AppContainer.js#L29-L31">pull state during rendering</a> but if you do that - oh my - what is the point of a centrally managed store? Isn't the value that your components are always up to date with the same state changes?
        <br />
        Sure, you could have a single subscriber at some very high level that's making the decision to pass new data to its children components, but then <strong>why does this component know about the store at all?</strong>
			</p>
		</li>
	</ol>
</section>
<section>
	<h2>The Part of Flux That Works For Me</h2>
	<strong>The strict actions.</strong>
	<p>
		Dear sweet <code>${user.favoriteDeity}</code>, standardized, universal, consistent actions are <strong>the best</strong>.
	</p>
	<figure>
		<img src="https://res.cloudinary.com/rockerest/image/upload/v1639130788/simple-message-workflow.jpg" />
		<img src="https://res.cloudinary.com/rockerest/image/upload/v1639130788/user-interface-workflow.jpg" />
		<figcaption>Every possible application workflow either starts with, ends with, or both starts and ends with a known action.</figcaption>
	</figure>
	<p>
		I honestly don't know why this pattern isn't used more often. When every part of an application that works on a "global" scale (think: routing, changing views, loading data) is codified and standardized, all you have to do to figure out how something works is follow the trail of actions it triggers.
	</p>
	<p>
		You might have noticed above that there are some actions that don't seem to correspond to something going into state. You're right, this architecture isn't about resolving everything to state, it's about connecting the disparate parts of your application together without complex inter-dependencies.
	</p>
	<p>
		This is the part of Redux/Flux that they get absolutely, gloriously correct: <strong>one</strong>, <em>single</em>, way for the application to encapsulate behavior.
	</p>
</section>
<section>
	<h2>The Best of Flux: Fluxbus</h2>
	<p>
		As mentioned above, the part of Redux/Flux that is good is the standardized messaging. The parts that are bad are myriad, but not the least are overly cumbersome data flow, restrictive storage, and the shotgun approach to calling subscribers.
	</p>
	<p>
		The problems boil down to the fact that the Flux architecture is prescribing too much. If the prescription was simply "All application changes are made by publishing a standard action" then it wouldn't have any of the problems.
	</p>
	<p>
		So, let's do just that. Fluxbus requires that <strong>all application changes are made by publishing a standard action through a central message bus.</strong> There's nothing more to it than that.
	</p>
	<h3>Example Implementation</h3>
	<pre><code class="javascript">// We're using rx to avoid having to implement our own observables
import { Subject } from "rxjs";

var bus = new Subject();
var LOGIN_REQUEST = {
	"name": "LOGIN_REQUEST",
	"username": "",
	"password": ""
};
var LOGIN_SUCCESS = {
	"name": "LOGIN_SUCCESS"
};
var LOGIN_FAILURE = {
	"name": "LOGIN_FAILURE",
	"problems": []
};

function publish( message ){
	return bus.next( message );
}

function subscribe( map ){
	// Returns an unsubscriber
	return bus.subscribe( ( message ) => {
		if( map[ message.name ] ){
			map[ message.name ]( message );
		}
	} );
}

subscribe( {
	"LOGIN_REQUEST": ( message ) => {
		fetch( "https://example.com/api/login", {
			"method": "POST",
			"body": JSON.stringify( {
				"username": message.username,
				"password": message.password
			} )
		} )
		.then( ( response ) => response.json() } )
		.then( ( json ) => {
			if( json.problems.length == 0 ){
				publish( LOGIN_SUCCESS );
			}
			else{
				publish( Object.assign(
					{},
					LOGIN_FAILURE,
					{ "problems": json.problems }
				) );
			}
		} );
	}
} );</code></pre>
	<p>
		What's going on here?
	</p>
	<pre><code class="javascript">// We're using rx to avoid having to implement our own observables
import { Subject } from "rxjs";

var bus = new Subject();
var LOGIN_REQUEST = {
	"name": "LOGIN_REQUEST",
	"username": "",
	"password": ""
};
var LOGIN_SUCCESS = {
	"name": "LOGIN_SUCCESS"
};
var LOGIN_FAILURE = {
	"name": "LOGIN_FAILURE",
	"problems": []
};</code></pre>
	<p>
		This is just setting up an <a href="https://rxjs-dev.firebaseapp.com/api/index/class/Subject">RxJS <code>Subject</code></a> and three actions. Note that this is <em>what a <a href="https://en.wikipedia.org/wiki/Software_bus">bus</a> is</em>. That's it. You can publish messages into it, and you can subscribe to published messages.
	</p>
	<pre><code class="javascript">function publish( message ){
	return bus.next( message );
}

function subscribe( map ){
	// Returns an unsubscriber
	return bus.subscribe( ( message ) => {
		if( map[ message.name ] ){
			map[ message.name ]( message );
		}
	} );
}</code></pre>
	<p>
		This is just wrapping the native Rx methods with a simple helper (in the case of <code>publish</code>) or our filter magic (in the case of <code>subscribe</code>). The subscriber takes a map of functions keyed on a message name. If that message name is seen, that subscriber is triggered.
	</p>
	<pre><code class="javascript">subscribe( {
	"LOGIN_REQUEST": ( message ) => {
		fetch( "https://example.com/api/login", {
			"method": "POST",
			"body": JSON.stringify( {
				"username": message.username,
				"password": message.password
			} )
		} )
		.then( ( response ) => response.json() } )
		.then( ( json ) => {
			if( json.problems.length == 0 ){
				publish( LOGIN_SUCCESS );
			}
			else{
				publish( Object.assign(
					{},
					LOGIN_FAILURE,
					{ "problems": json.problems }
				);
			}
		} );
	}
} );</code></pre>
	<p>
		This sets up a subscription that waits for a login request. When it sees one, it tries to log in with the provided username and password. If that login succeeds, this publishes a <code>LOGIN_SUCCESS</code> message. If the login fails, it publishes a <code>LOGIN_FAILURE</code> message.
	</p>
	<p>
		Of course, you might want to organize this a bit better.
		<br />
		You could put all your <code>messages</code> in a folder. You could put all the stuff that sets up and interacts with the bus in a file like <code>MessageBus.js</code>. You could put all your universal responses (like to logging in) in action files like <code>actions/Authentication.js</code>.
		<br />
		Then, when your application starts up, it might look like this:
	</p>
	<pre><code class="javascript">import { getBus, publish, subscribe } from "./MessageBus.js";

import { registrar as registerAuthenticationActions } from "./actions/Authentication.js";

import { message as REQUEST_LOGIN } from "./messages/REQUEST_LOGIN.js";

var bus = getBus();
var partiallyAppliedPublish = ( message ) => publish( message, bus );
var partiallyAppliedSubscribe = ( map ) => subscribe( map, bus );

registerAuthenticationActions( partiallyAppliedSubscribe, partiallyAppliedPublish );

// More stuff?
/*
	Automatically log in with a demo user on startup?

	publish( Object.assign(
		{},
		REQUEST_LOGIN,
		{ "username": "demo", "password": "demopwd" }
	), bus );
*/</code></pre>
</section>
<section>
	<h2>Footgun</h2>
	<p>
		This architecture is not for the faint of heart. A big part of the draw of something like Redux or Flux is <em>it tells you what to do all the way down to the UI layer</em>. Then, you're expected to use React for the UI, and that framework will also tell you what to do.
	</p>
	<p>
		If you don't feel like there's enough structure here for you, you're probably right.
	</p>
	<p>
		<strong>Fluxbus</strong> expects that you will exercise careful, defensive programming techniques. Because you have the freedom to make an enormous mess of things, you are expected not to.
	</p>
	<p>
		Note - of course - there's no talk of data stores or dispatchers or reducers. Those are all the unnecessarily cumbersome bits of Redux/Flux. You want a data store? Great! Find one you like, or just use the many extremely robust storage options built into the browser. Do you need a way to reduce changes to data into your store? I suggest <code>Object.assign( {}, previous, new );</code> but literally the whole world is open to you.
	</p>
</section>
