---{
		"id": "77f63080-5459-4a71-a9fc-132531859504",
    "authors": [
        "trandolph"
    ],
    "title": "Tabs vs. Spaces: One Right Answer",
    "publish": "2018-08-29T10:00:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
  <h2>Déjà Vu</h2>
  <p>
    Yes, I've talked about this before. I wrote that <a href="https://log.rockerest.com/post/invisibilia-the-code-within.html">"Whitespace Is The Invisible Code Inside Your Code"</a> and that <a href="https://log.rockerest.com/post/the-seven-things-that-make-a-senior-engineer.html">"code is not primarily for doing things with code or computers; it's for communicating intent to the next person"</a>. Two data points is a line, three data points is a trend.
  </p>
  <p>
    This is a trend I'm fully upfront about, though. I believe software is <strong>not</strong> necessarily about doing things faster (<a href="https://log.rockerest.com/post/the-seven-things-that-make-a-senior-engineer.html">"Quicker is not necessarily better. Viva Code Liberte."</a>), nor is it about getting to an arbitrary milestone faster (<a href="https://log.rockerest.com/post/convention-syndrome-or-how-to-be-mediocre.html">"despite being able to present a prototype in a short amount of time — the developer is rarely able to create a truly innovative end result."</a>).
  </p>
  <p>
    I think that's all of the self-referencing I'm going to do. Come with me while I explain why the Tabs vs. Spaces argument is a symptom of narcissism and should be dismissed. There <strong>is</strong> a correct choice, and those who argue against it are selfish.
  </p>
</section>
<section>
  <h2>Dumb Arguments</h2>
  <p>
    First of all, let's avoid the pitfall of arguing about <em>keyboards</em>. When we talk about tabs versus spaces <strong>everyone is pressing the Tab key on their keyboard.</strong> If you're a spaces person and you're mashing on the spacebar to get the right indent level you are an actual psychopath.
  </p>
  <p>
    This argument is fundamentally about <em>what your editor does when you press the Tab key</em>. So let's define the terminology here:
  </p>
  <ul>
    <li><strong>spaces</strong> is when you press the Tab key and the editor inserts some number of space characters (usually called a <em>soft tab</em>).</li>
    <li><strong>tabs</strong> is when you press the Tab key and the editor inserts one tab character (usually called a <em>hard tab</em>).</li>
  </ul>
  <p>
    Secondly, let's not get into talking about file sizes. <em>*heavy sigh*</em>. Nobody cares. If the process by which you are delivering this code is filesize-sensitive and you are not already using some kind of compression algorithm which would collapse all whitespace into a single replacement, <strong>you are doing your deployments incorrectly</strong>. Quick tip here: gzip will do this for web applications and your web server supports it and you need to turn it on.
  </p>
  <p>
    Is there anything else? We're not talking about file size and we're not talking about what you mash your meatsicles into. We're talking about editor settings.
  </p>
</section>
<section>
  <h2>The Only Good Argument</h2>
  <p>
    The only good argument in this conversation is the exact same argument that should be made for every other code decision: what is the best choice for developer quality of life and code readability in the future?
  </p>
  <p>
    I used to be staunchly spaces. The idea is basically: spaces look the same for every developer. The way the code is authored is the way it's displayed to every developer forever. This is a garbage argument (see how I snuck this in here instead of in the bad arguments section?). <strong>The author should not be determining how the future reader looks at the code</strong>. Do you tell other people what their editor theme should be? Do you tell them what their font size should be? If you do, you're a fascist and goodness please quit your job to save the rest of us from you.
  </p>
  <p>
    How code <strong>looks</strong> to each developer is something that is personal to each developer, and it's not the author's business to decide that.
  </p>
  <aside>
    Note that I'm not talking about languages that require certain forms of whitespace in order to parse/compile properly. This is absolute madness (looking at you, Python), because it puts <strong>semantic</strong> meaning on invisible characters. The same rules apply (you should be choosing the option that allows developer freedom of choice), regardless, but since languages like Python put semantics on whitespace, they have rules like PEP-8 that enforce spaces-only. This is extremely developer-hostile. Anyway, we're talking about sane languages where whitespace is non-strict.
  </aside>
  <p>
    So, let's break down the information that we <strong>have</strong> to encode into the source:
  </p>
  <ol>
    <li>The line needs indentation.</li>
    <li>Which level of indentation.</li>
  </ol>
  <p>
    Note here that when I say that we "have" to encode this into the source, I come to that conclusion because this is the choice that lends itself best to <em>future code readability and developer quality of life</em>. You may notice some kind of pattern emerging here.
  </p>
  <p>
    Sure, you don't <em>have</em> to encode indentation (yes/no) and indentation (amount) into your source code.
    <br />
    You also don't <em>have</em> to encode line breaks into your source code, but I suspect that you put line breaks in there.
    <br />
    So you're already on the "this-is-helpful-for-readability" train. If you weren't, you'd be writing all your code on a single line. You know. Like a monster.
  </p>
</section>
<section>
  <h2>The Choices</h2>
  <p>
    <strong>Spaces</strong> encodes three things into the source:
  </p>
  <ol>
    <li>The line needs indentation.</li>
    <li>Which level of indentation.</li>
    <li>How to display the indentation.</li>
  </ol>
  <p>
    It's that third item that's the problem. Whether a developer wants to display each level of indentation as 4 spaces or 2 spaces or 9 spaces or a Fibonacci sequence of spaces for each level; that's up to them. The only way to change how a bunch of spaces looks is by adjusting the font size, which has all sorts of other - practically unrelated - impacts on the reader.
  </p>
  <p>
    Consider, instead, the <strong>tab</strong> character. This encodes only two things into the source:
  </p>
  <ol>
    <li>The line needs indentation.</li>
    <li>Which level of indentation.</li>
  </ol>
  <p>
    It is up to the editor viewing the source - and only the editor - to determine how to display that tab. Just like it's up to the editor how to colorize the code. Just like it's up to the editor to determine what font size to display the code in.
  </p>
</section>
<section>
  <h2>The Illusion of Choice</h2>
  <p>
    So really, there isn't a choice here. If you choose spaces, you are explicitly choosing to enforce an authored display decision on future readers. If you choose tabs, you are choosing to encode only the pertinent information and let the future reader decide how to display that.
  </p>
  <p>
    In other words, "<strong>spaces</strong>" is a result of a mindset that puts the author - at the time they write the code - at the center: What does the <strong>author</strong> think this should look like? Spaces enforces that decision to everyone else; an outpouring of narcissism that reverberates across time.
  </p>
  <p>
    "<strong>tabs</strong>" is a result of a mindset that believes future developers can make their own decisions about how they look at code. It's a live and let live mindset that every developer should strive for: "Here's a thing that works for both of us, even though we've both made different decisions layered on top of it."
  </p>
  <p>
    Choose <strong>tabs</strong>. Viva Code Liberte.
  </p>
</section>
