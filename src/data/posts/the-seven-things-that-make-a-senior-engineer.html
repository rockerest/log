---{
		"id": "a7f12da6-5e61-4ddd-a808-5fc4db082b2d",
    "authors": [
        "trandolph"
    ],
    "title": "The Seven Things That Make a \"Senior\" Engineer",
    "publish": "2018-03-22T23:30:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
  <p>
    Over the past few years, I've been asked this question - "what is a Senior Front End Engineer?" - roughly a half-dozen times. Each time, my answer has evolved a bit more, but the same basic points have always been present. Sometimes the question is more specific to JavaScript (e.g. "Senior JavaScript Engineer"). My answer is roughly the same for all of these instances.
  </p>
  <p>
    I have seven items that I believe define a Senior Front End Engineer, although with the exception of item number two, these qualifications could  apply to any senior role at all.
  </p>
</section>
<section>
  <h2>Foresight</h2>
  <p>
    In the interest of context, I used to say my number one item for a senior engineer was "deep knowledge of JavaScript fundamentals." Unfortunately, when you say that these days, people who used React for a year and really, <strong>really</strong> like the <code>let</code> keyword and arrow functions think they fall into the "highly experienced with fundamentals of JavaScript" camp, and that's not often the case.
  </p>
  <p>
    So, in this new era of "script-kiddie-turned-programmer" my number one thing for a Senior Front End Engineer is foresight. Unfortunately, foresight isn't something you can drop into a Github repo for review, so it's pretty tough to guage.
  </p>
  <p>
  An example: given a choice between a monolith and highly decoupled task-specific tools, do you choose the plug-and-play monolith or do you choose the task-specific tool?
    <br />
    In more concrete terms: Do all of your integrations start with <code>[insert-tool-du-jour]-</code> or are they standalone?
    <br />
    To me, choosing the standalone tool demonstrates foresight. If your entire app is <code>react-this</code>, <code>react-that</code>, <code>react-router</code>, <code>react-dom</code>, what happens when one dependency has a breaking change that you can't update, or the underlying technology stops meeting your needs? Those decisions result in <strong>complete</strong> system rewrites instead of highly focused slice replacements.
  </p>
  <p>
    I'm speaking from experience here, both having been in this wasteland myself, and looking in from the outside: An adjacent team is rewriting their product (previously Angular 1.x) because they can no longer sustain the monolith and all of its snaking and intertwined dependencies. However, they chose to rewrite it in React, and have dived back into the deep end of a highly coupled architecture, where everything depends on everything else.
  </p>
  <p>
    There are certainly tradeoffs to either approach, but the benefit of the "monolith" approach is usually "we can do so much so quickly!" Unfortunately that also means everything that's done is often torn down wholesale and rewritten because it becomes untenable.
  </p>
  <p>
    <strong>#1: Foresight</strong>. Quicker is not necessarily better. Viva Code Liberte.
  </p>
  </section>
  <section>
  <h2>Deep knowledge of the platform</h2>
  <p>My number two qualification is still deep knowledge of the ecosystem, but I've expanded it from just JavaScript to include HTML and CSS. These three things are <strong>equal</strong> parts of the task of developing for the front end and many (most) devs have explicitly made the decision to focus on just JavaScript. This lack of understanding of the entire platform (the web is a platform, after all), has led to a lot of tangential problems (like people inventing new ways to only write JS but still get CSS and HTML out of it by jumping through ridiculous hoops).
  </p>
  <p>
    However, there are real, tangible problems that this lack of depth causes. I recently witnessed a developer spend a week or so writing an autocomplete component in JavaScript. When it was announced and ready for review I asked - honestly - why we weren't using the <strong>native</strong> autocomplete element built into every browser. We eventually just went with the one we had built because it was "easier to style" and - more or less - we had sunk the cost into it already.
  </p>
  <p>
    Demonstrating deep knowledge of the platform you're ostensibly a professional in is both critical to being able to claim mastery, and critical to being able to make wise decisions that build on solid, standardized groundwork.
  </p>
  <p>
    <strong>#2: Deep knowledge of the web platform, in all it's parts</strong>. Build on the shoulders of giants.
  </p>
  </section>
<section>
  <h2>Communication</h2>
  <p>
    My number three qualification is basically "soft skills", but I tend to focus on communication, primarily written.
  </p>
  <p>
    Being a developer is mostly about communicating. Yes, there's a part where you write code, but <strong>even that code</strong> is not primarily for doing things with code or computers; it's for communicating intent to the next person, even if that next person is you, later.
  </p>
  <p>
    If you accomplish the "now" goal but do so in a way that nobody understands in two months or two years (when the "now" goal has turned into the "that-was-then" goal), you've failed at your job as a developer.
  </p>
  <p>
    A relevant aside: this ties back into the number one qualification - having foresight means building code that meets the "now" goal <strong>and</strong> doesn't need to be deleted and re-written for the "new-now" future goal.
    <br />
    I digress.
  </p>
  <p>
    Communication is a deep topic with many facets, but being able to actively retrospect on decisions that you've made and determine the outcomes of those decisions - even years down the road - is critical.
    <br />
    Communicating that introspection in a coherent way is crucial.
    <br />
    After significant incidents, I try to write up a retrospective: what happened (summarize, describe the intent of the document, give background and context), why did that happen, what steps were taken to resolve that at the time, and how can it be avoided in the future?
    <br />
    Break problems down using a root cause analysis method like the <a href="https://en.wikipedia.org/wiki/5_Whys">5 Whys</a>.
  </p>
  <p>
    Too many times (too many to count), developers give the facts: "This broke. I fixed it. It is not broken." This doesn't help anyone in the future, and it doesn't delve into the hard parts of problems which typically aren't facts.
    <br />
    Case in point: in a previous retrospective document, I wound up suggesting preemptive skills testing and company-provided training on weak subjects. Without looking deeply at the - inevitably human - failures and breaking them down to their constituent parts, there's no path forward for learning and growth. Being able to communicate that is critical to both personal and professional growth.
  </p>
  <p>
    What I'm attempting to get across - perhaps poorly, in an ironic twist of fate - is that communication about <strong>all</strong> kinds of things is important. Being able to communicate facts and being able to communicate truths - even if complex or painful, is a sign of maturity and depth. Giving <strong>every</strong> detail empowers others to extrapolate and form patterns and learn on their own.
  </p>
  <p>
    <strong>#3: Superb communication skills</strong>. Being an engineer is communication. Being a <strong>coder</strong> is writing code.
  </p>
  </section>
  <section>
  <h2>Hype-Aversion</h2>
  <p>
    My number four connects back to my number one at a foundational level.
    <br />
    Senior engineers are hype-averse. Not hype-neutral. Hype-averse.
    <br />
    A senior engineer is actively suspicious of the most popular technology. They carefully evaluate it without buying into dangerously harmful <a href="https://en.wikipedia.org/wiki/Cargo_cult_programming#Cargo_cult_software_engineering">cargo-cultism</a> and hero worship. They understand the wide-reaching implications of technology decisions on a business on the scale of 10 years, not two.
  </p>
  <p>
    <strong>#4: Acutely hype-averse</strong>.
  </p>
  </section>
  <section>
  <h2>Craftspersonship</h2>
<p>
  My number five qualification for senior engineers is care about the product of their energy. Another way to put this is being a craftsperson.
  </p>
  <p>
    Writing front end software is all about catching edge cases. Browsers and mobile phone environments are this crazy land where there are practically infinite situations that the user might be in, and practically infinite ways the user might not use the code in the way that was intended.
    <br />
    A craftperson knows that they need to handle all of the <strong>unexpected</strong> ways things could go wrong - not just the one way things could go right.
    <br />
      A craftperson pays attention to - and cares about - the details. Spelling and grammar matter, color contrast matters (what if the user is in direct sunlight?), font size matters (it should be larger than your team of 25 year olds thinks it should be).
  </p>
  <p>
    Something I see with alarming frequency is code copy-pasted around codebases with accompanying information unchanged.
    <br />
    It happens all the time in tests: the test description ("should do x") is unchanged from the place it was copied from. Not only is this misleading when the tests are running / failing / being refactored, but it's indicitive of <em>carelessness</em>.
    <br />
    It happens in application code, too: some expression is coded where it operates on an object property, but there's no check that the variable is actually an object, which would lead to fatal thrown errors.
  </p>
  <aside>
    Don't send me messages about static and/or strong type systems; you're wrong.
  </aside>
  <p>
    I often hear things like "I don't care about [insert something small relating to software development], I just want to be able to get more work done." This is the mark of someone who lacks the spirit of a craftsperson. Caring about every detail of the thing you are creating and putting your name on elevates that work to a higher level, and that higher level is where it <strong>should</strong> be.
  </p>
  <p>
    <strong>#5: The mindset of a craftsperson</strong>. Caring about the tiny details and the big picture.
  </p>
</section>
<section>
  <h2>Bubble Awareness</h2>
  <p>
    My number six qualification for senior engineers is being aware of their own bubble and actively trying to burst it.
    <br />
    This one is pretty straightforward: Our field is mostly filled with white men ages 20-40 in the middle or upper-middle income range. These people are generally healthy or have medical support to the point where their health doesn't have a regular daily impact on their lives. These people tend to write software for themselves.
    <br />
    I mentioned color contrast and font size above. Know your target audience or even better: consider the needs of everyone, not just the target demographic. Text should be larger and higher contrast for all but the absolute healthiest eyes (not to mention - of course - those who can't see at all).
  </p>
  <p>
    I was recently involved in a discussion about what the default body font size should be in our production applications, and 13px was legitimately floated as a popular option.
  <br />
    For a person whose eyesight is a little bad, maybe using our software outside, 13px would be virtually unreadable.
  </p>
  <hr />
  <p>
    There are a lot of young developers - many of whom recently graduated from bootcamps. Young developers - some of them given the crash course in getting a product out as fast as possible - tend to reach for the tools that they know other people - or themselves - have used in the past. Often these tools are not the most efficient for the end user.
    <br />
      Many end users (probably a super-majority, if you're being honest with yourself) do not use your software on late-model Macbook Pro computers with 16GB or more of RAM. They're probably using a Windows computer from sometime in the last 6 years and it might have 8GB of RAM if you're incredibly lucky.
    <br />
      A web application that unpacks to 30MB and uses 1GB of memory during runtime is not just unacceptable, it's actively user-hostile.
  </p>
  <p>
    <strong>#6: Aware of the limitations of their own bubble</strong>. Actively trying to expand their perspective.
  </p>
  </section>
  <section>
  <h2>Teacher</h2>
  <p>
    This qualification of senior engineers relates to both my third and fifth qualifications: If you care about your craft and you're a good communicator: Share.
    <br />
      Put together the bare minimums on something and throw some slides together and demonstrate something. Sit down with a group who's interested and talk about something. Sit down with an individual and go over something they've asked you about.
    <br />
      Teach.
     <br />
      Teach is the important word here. I've seen a lot of demos where we were in the person's code editor or some third party tool the entire time.
    <br />
      Teaching means breaking something down into foundational components and presenting those. Smart people will build on a solid foundation, but virtually anyone will get lost as you flip through a code implementation.
    <br />
      Being able to break a complex (or even a simple) idea down into a distillate is crucial to being senior. The goal of a senior is not to be king of the party, the goal of a senior is to bring everyone else up to their level.
  </p>
  <p>
    <strong>#7: A sharing, teaching spirit</strong>. Senior developers exist to anchor and support the more junior developers.
  </p>
</section>
<section>
  <p>
    To me, these are the seven things that make a developer into a senior engineer:
  </p>
  <ol>
    <li>
      <strong>Foresight</strong>. Quicker is not necessarily better. Viva Code Liberte.
    </li>
    <li>
      <strong>Deep knowledge of the web platform, in all it's parts</strong>. Build on the shoulders of giants.
    </li>
    <li>
      <strong>Superb communication skills</strong>. Being an engineer is communication. Being a <strong>coder</strong> is writing code.
    </li>
    <li>
      <strong>Acutely hype-averse</strong>.
    </li>
    <li>
      <strong>The mindset of a craftsperson</strong>. Caring about the tiny details and the big picture.
    </li>
    <li>
      <strong>Aware of the limitations of their own bubble</strong>. Actively trying to expand their perspective.
    </li>
    <li>
      <strong>A sharing, teaching spirit</strong>. Senior developers exist to anchor and support the more junior developers.
    </li>
  </ol>
  </section>
