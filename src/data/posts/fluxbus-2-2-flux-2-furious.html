---{
	"id": "0060a7c0-b235-49a7-a083-4ea5245ada5e",
    "authors": [
        "trandolph"
    ],
    "title": "Fluxbus 2: 2 Flux 2 Furious",
    "publish": "2020-07-31T15:30:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
		<h2>This again.</h2>
		<p>
			I've talked about <a href="https://log.rdl.ph/post/fluxbus-an-application-architecture-pattern.html">Fluxbus in the past</a>.
			If you haven't read about Fluxbus, I <em>(not very)</em> humbly suggest it's the application architecture you need but have never heard of.
		</p>
		<p>
			Astute readers might notice that I bemoan one of the worst things about Flux:
		</p>
		<blockquote>
			<strong>Every single listener is called for every single change</strong>
			<p>[…]</p>
			<p>
				Since the subscription pattern in Redux is <code>store.subscribe( someCallback )</code>, there is no way to filter how many functions are run when the state updates.
				<br />
				Imagine a moderately complex application that has a state value like <code>{ messages: { loading: true } }</code> and a few hundred subscribers.
				<br />
				How many of those subscribers care that the messages stop loading at some point? Probably 1. Wherever your chat module is, it's probably the <strong>only</strong> place that cares when the messages stop loading and it can hide a spinner.
				<br />
				But in this subscription paradigm, all of your few hundred subscribers are called, and the logic is checked to determine if they need to update. The inefficiency is mind-boggling.
			</p>
			<p>
				Again, Flux doesn't quite have the exact same problem: not only because of the fact that you can have multiple stores for specific intents, but also because it doesn't prescribe any particular subscription pattern.
				<br />
				You <strong>could</strong> implement a pre-call filter that allows subscribers to only be called for particular actions that they care about, but this is so far off the beaten path it's never been seen by human eyes before.
			</p>
		</blockquote>
		<p>
			And then go on to suggest the alternative - <em>Fluxbus</em> - which has an implementation like this:
		</p>
		<blockquote>
			<br />
			<pre><code class="javascript">function publish( message ){
	return bus.next( message );
}

function subscribe( map ){
	// Returns an unsubscriber
	return bus.subscribe( ( message ) => {
		if( map[ message.name ] ){
			map[ message.name ]( message );
		}
	} );
}</code></pre>
			<p>
				This is just wrapping the native Rx methods with a simple helper (in the case of <code>publish</code>) or our filter magic (in the case of <code>subscribe</code>). The subscriber takes a map of functions keyed on a message name. If that message name is seen, that subscriber is triggered.
			</p>
		</blockquote>
		<p>
			Ladies and gentlefolk, this is what's called a <em>Minimum Viable Product</em>.
		</p>
	</section>
	<section>
		<h2>Explain yourself.</h2>
		<p>
			The problem here is that in a <strong>worst case</strong> scenario, this is exactly as bad as the Flux pattern where it calls every subscriber for every change.
			<br />
			Granted, that's the <em>worst case</em> behavior, compared to Flux's intended average behavior, but it's still not great.
		</p>
		<p>
			Let me explain what I mean by worst case.
			<br />
			Let's say you have 1000 bus messages that you'd like to subscribe to. In a normal application, you might subscribe to 5 in one component, and 10 in another, and so on.
			This might result in close to 200 different subscribers.
			<br />
			As a refresher, you might subscribe like this:
		</p>
		<pre><code class="javascript">subscribe( {
	"SOMETHING_I_CARE_ABOUT": ( message ) => { ... },
	"SOMETHING_ELSE": ( message ) => { ... },
	"ANOTHER_THING": ( message ) => { ... },
	"ONE_MORE_THING": ( message ) => { ... },
} );</code></pre>
		<p>
			In practice, you've added <strong>one</strong> subscriber, but it will hand off control to any of those four handlers when the bus recieves any of those four events.
			<br />
			Pretty decent, but the outer call is still dumb. There's no filtering at the top level, so every time you call <code>subscribe</code>, that's another outer subscriber that will get called on <strong>every</strong> message through the bus.
		</p>
		<p>
			Again, I want to reiterate: this is significantly better than binding into every component and calling every handler on any message (or any data change, in Flux terms). Once a function like this is called, the Message Bus will determine that none of the message names match, and it won't call the handlers.
		</p>
		<p>
			At this point, I'm sure you can imagine the worst case scenario. I'll rewrite the above:
		</p>
		<pre><code class="javascript">subscribe( {
	"SOMETHING_I_CARE_ABOUT": ( message ) => { ... }
} );

subscribe( {
	"SOMETHING_ELSE": ( message ) => { ... }
} );

subscribe( {
	"ANOTHER_THING": ( message ) => { ... }
} );

subscribe( {
	"ONE_MORE_THING": ( message ) => { ... }
} );</code></pre>
		<p>
			Now we're close to Flux territory. While we're not running expressions inside component bindings to check whether the update is relevant to us or not, we're still calling every outer subscriber for <strong>every single</strong> message sent through the bus.
			<br />
			Reiterating one last time: even the worst case scenario here avoids unnecessary component cycles by never calling into the handlers if the message doesn't match.
			<br />
			Fluxbus with both hands tied behind its back still beats Flux on a normal day. But it's not ideal.
		</p>
	</section>
	<section>
		<h2>Fix it.</h2>
		<p>
			The key here is that Fluxbus was just the absolute thinnest of wrappers around an RxJS Subject. It's barely even there. Here's the code again:
		</p>
		<pre><code class="javascript">function publish( message ){
	return bus.next( message );
}

function subscribe( map ){
	// Returns an unsubscriber
	return bus.subscribe( ( message ) => {
		if( map[ message.name ] ){
			map[ message.name ]( message );
		}
	} );
}</code></pre>
		<p>
			This is the absolute minimum extra code possible to implement subscriptions-by-message-name.
		</p>
		<p>
			We can do a lot better by being just a little more clever.
		</p>
		<p>
			The basic premise is that a Fluxbus instance can track handlers separately from handling inbound messages. So when a consumer calls <code>subscribe</code>, the internals of Fluxbus turn the message-name-to-handler mapping into a message-name-to-<em>identifier</em> mapping. Then it pushes the handler into a dictionary of all handlers and it's done.
			<br />
			Of course, that's not quite the whole picture - you need to reasonably handle unsubscriptions, too, and there are other little optimizations you can do along the way.
			<br />
			For the sake of our brains here, I'll present the MVP for this - sans abstractions and optimizations.
		</p>
	</section>
	<section>
		<h2>It gives us the code or it gets the hose again.</h2>
		<pre><code class="javascript">var bus = new Subject();
var handlers = {};
var listeners = {};

bus.subscribe( ( message ) => {
	( listeners[ message.name ] || [] )
		.map( ( id ) => handlers[ id ] )
		.forEach( ( handler ) => {
			handler( message );
		} );
} );

export function publish( message ){
	bus.next( message );
}

export function subscribe( messageMap ){
	var names = Object.keys( messageMap );
	var callbacks = Object.values( messageMap );
	var idMap = [];

	callbacks.forEach( ( cb, i ) => {
		let name = names[ i ];
		let id = uuidv4();

		handlers[ id ] = cb;

		if( listeners[ name ] ){
			listeners[ name ].push( id );
		}
		else{
			listeners[ name ] = [ id ];
		}

		idMap.push( { name, id } );
	} );

	return () => {
		idMap.forEach( ( { name, id } ) => {
			delete handlers[ id ];

			if( listeners[ name ] ){
				listeners[ name ] = listeners[ name ].filter( ( listener ) => listener != id );
			}

			if( listeners[ name ].length == 0 ){
				delete listeners[ name ];
			}
		} );
	};
}
</code></pre>
	</section>
	<section>
		<h2>My eyes.</h2>
		<p>
			You said you'd give me the hose if I didn't give you the code.
		</p>
	</section>
	<section>
		<h2>What's happening here?</h2>
		<p>
			Let's break it down.
		</p>
		<pre><code class="javascript">var bus = new Subject();
var handlers = {};
var listeners = {};

bus.subscribe( ( message ) => {
	( listeners[ message.name ] || [] )
		.map( ( id ) => handlers[ id ] )
		.forEach( ( handler ) => {
			handler( message );
		} );
} );</code></pre>
		<p>
			In the first section, we set up an RxJS Subject as our bus, just like before. We also define two variables that are going to cause all of this to be "singletonish" which is a very technical term for "singleton-y" things. It's a closure, basically.
			<br />
			Then, we immediately subscribe to the bus. This is the only bus subscriber. It will determine which handlers to trigger when a message comes in.
			<br />
			Roughly, this subscriber goes something like this:
		</p>
		<ol>
			<li>Give me all the IDs of listeners for this message name</li>
			<li>Convert all those IDs into their real handler functions</li>
			<li>Call each handler function with the message</li>
		</ol>
		<pre><code class="javascript">export function publish( message ){
	bus.next( message );
}</code></pre>
		<p>
			Then we have our normal <code>publish</code> function.
		</p>
		<p><sup>boring 🥱</sup></p>
		<pre><code class="javascript">export function subscribe( messageMap ){
	var names = Object.keys( messageMap );
	var callbacks = Object.values( messageMap );
	var idMap = [];

	...
}</code></pre>
		<p>
			Then, we get to the real magic: <code>subscribe</code>.
			<br />
			<code>subscribe</code> still accepts a dictionary that maps a message name to a handler function to be called.
			But now, we immediately split that map into the name keys we're listening for and the handler values to be called.
			It's going to be very convenient later to have these as discrete arrays.
			We need a way to keep track of the mappings we're going to create here, so we also initialize an empty array to store mappings.
		</p>
		<pre><code class="javascript">export function subscribe( messageMap ){
	...

	callbacks.forEach( ( cb, i ) => {
		let name = names[ i ];
		let id = uuidv4();

		handlers[ id ] = cb;

		if( listeners[ name ] ){
			listeners[ name ].push( id );
		}
		else{
			listeners[ name ] = [ id ];
		}

		idMap.push( { name, id } );
	} );

	...
}</code></pre>
		<p>
			The next chunk begins by iterating over each of our handler functions and grabbing the message name associated with it and generating a new unique ID for that handler.
		</p>
		<blockquote>
			Say there fella, couldn't this be
			<pre><code class="javascript">Object
	.entries( messageMap )
	.forEach( ( [ name, cb ] ) => { ... } );</code></pre>
			to avoid two extra variables and two iterations over the object to extract them?
		</blockquote>
		<sub><em>……………………………… stop yelling at me i'm the internet's sweetie pie</em></sub>
		<p>
			Once we have a unique ID we immediately store our handler in a dictionary associated to it.
		</p>
		<p>
			Then, we check our global listeners object for the message name. If we're already listening to that message name, we push our handler ID into the list. If we're not, we just create a new list.
		</p>
		<p>
			To keep track of all our mappings in <strong>this</strong> subscriber, we push an object into the list of ids that includes both the message name and the handler ID.
		</p>
		<pre><code class="javascript">export function subscribe( messageMap ){
	...

	return () => {
		idMap.forEach( ( { name, id } ) => {
			delete handlers[ id ];

			if( listeners[ name ] ){
				listeners[ name ] = listeners[ name ].filter( ( listener ) => listener != id );
			}

			if( listeners[ name ].length == 0 ){
				delete listeners[ name ];
			}
		} );
	}
};</code></pre>
		<p>
			Finally, we return an unsubscribe function.
			When a consumer calls <code>subscribe</code> with a map of message names and their handlers, they will expect to receive an unsubscriber that unsubscribes <strong>all</strong> of their handlers at once just like they passed in a single dictionary.
		</p>
		<p>
			So we take our handy array of <code>{ "name": message.name, "id": handlerId }</code> mappings and we loop over the whole thing.
			<aside>
				Note that because we returned a function that includes a reference to <code>idMap</code>, we created another closure. That unsubscribe function will retain a reference to its own unique <code>idMap</code> list.
			</aside>
			We delete the handler associated with the ID we're iterating over. Then, we remove the listener ID from the list of all the listeners for that message name.
			<br />
			As a last cleanup check, if that message name no longer has any associated listeners, we just delete it from the dictionary of message names that we check in the bus subscriber.
		</p>
	</section>
	<section>
		<h2>Do math.</h2>
  <p>
    If we let <code>n</code> be the total calls to the <code>subscribe</code> and <code>x</code> be the total subscriptions for a given message (like <code>"SOME_EVENT": ( message ) => { ... }</code>), then...
  </p>
		<p>
			The previous version of Fluxbus was <code>O(n + x)</code> for every published message.
			<br />
			That is: if your application calls <code>subscribe</code> 100 times, and in just one, single instance of those calls you subscribe to <code>SOME_EVENT</code>, there will be 101 function calls when a single <code>SOME_EVENT</code> is published into the bus.
		</p>
		<p>
			This new version of Fluxbus is <code>O(x + 1)</code> for every published message.
			<br />
			That is: if your application calls <code>subscribe</code> 100 times, and in just one, single instance of those calls you subscribe to <code>SOME_EVENT</code>, there will be 2 function calls when a single <code>SOME_EVENT</code> is published into the bus.
			<aside>
				Note that the <code>+ 1</code> is the parent bus subscription that manages every listener. If you have 1000 listeners to a single message, it will result in 1001 calls, not 2000.
			</aside>
		</p>
	</section>
	<section>
		<h2>Done?</h2>
		<p>
			Of course not.
		</p>
		<p>
			The immediately obvious next step is improving this code. You can see such an improvement in <a href="https://gitlab.com/rockerest/taproot">the library I built to reduce my own application boilerplate</a>. I've very cleverly named the implementation <a href="https://gitlab.com/rockerest/taproot/-/blob/master/lib/MessageBus.js">MessageBus</a>.
		</p>
		<p>
			Of course then there's syntactical stuff like what you were yelling at me about up above.
			<br />
			Code is never finished.
		</p>
		<p>
			What do you think about Fluxbus 2?
		</p>
	</section>
