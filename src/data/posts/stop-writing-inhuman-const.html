---{
		"id": "447dbbd1-2203-4eb2-ba2a-4f123cd280f7",
    "authors": [
        "trandolph"
    ],
    "title": "Stop Writing Inhuman const",
    "publish": "2020-01-31T13:45:00-06:00",
    "status": "published",
    "type": "long",
    "rssExclusive": false
}---
<section>
  <p>
    All code is for humans. In fact, the only value in higher-order programming languages is communication with other human beings.
    <br />
    <br />
    Phrased as the reverse, code is not for computers. If code were for computers, binary would be the only acceptable programming language.
    <br />
    <br />
    Code is for humans, as I have <a href="https://log.rockerest.com/post/invisibilia-the-code-within.html">written</a> <a href="https://log.rockerest.com/post/the-seven-things-that-make-a-senior-engineer.html">about</a> <a href="https://log.rockerest.com/post/tabs-vs-spaces-one-right-answer.html">at</a> <a href="https://log.rockerest.com/post/why-i-use-web-components.html">length</a> before.
  </p>
</section>
<section>
  <h2>We Forgot That Words Have Meanings</h2>
  <p>
    File this one as another of the tsunami of reasons I have to regret being a software engineer. Somewhere along the line, the siren-call of tech above all else washed away the plain truth that other human beings should come first, no matter what the tech is or how it makes you feel when you're writing the code.
    <br />
    Words mean things.
    <br />
    JavaScript's <code>const</code> is a shorthand keyword for the word "constant." Constant has a very specific meaning in the world: it is things that do not change.
    <br />
    Even <strong>if</strong> you're using <code>const</code> in a language where the variable is truly constant in every sense of the word (which JavaScript is not, in most cases), you unequivocally have other tools to store values, and you use them regularly. You use them regularly because the code you write is intended to communicate with the next person reading it.
    <br />
    The message the next reader consumes is either "this value is constant" or "this value is not constant." That's because "constant" is an English word that has a meaning, and any programming language using a keyword anything like <code>const</code> is leveraging the real English word to provide context and meaning to the next reader.
  </p>
</section>
<section>
  <h2>King <code>var</code></h2>
  <p>
    <code>var</code> is shorthand for "variable." How beautifully succinct! How flexibly true!
    <br />
    For almost all practical purposes, and in almost all cases, a variable is what's desired, and a variable is what's being used. If you have an application where every single code path is purely deterministic in every sense: congratulations, you have a program with no data or user input. It is in this strange scenario that you might be forgiven for using <code>const</code> exclusively. After all, nothing can be dynamic if there is no input, and the return values from the function calls are already known ahead of time (if you're even using functions; what's the point of encapsulation and modularization when the entire program is hard coded?).
    <br />
    <code>var</code> should be your go-to, always-on, default.
	</p>
	<h2>Edgy <code>let</code></h2>
  <p>
		JavaScript has a very powerful scoping paradigm called "Lexical Scoping." If that gives you unrest, I strongly encourage you to <a href="https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/scope-closures/ch2.md">deeply read this book by Kyle Simpson</a>. Lexical scope is basically just "scope you can read by looking at the source."
    <br />
    Some people think that lexical scoping is bad. Because of this, they petitioned to have two new types of variable declaration keywords added to JavaScript (ECMAScript). <code>let</code> is one of these. <code>let</code> uses "Block Scoping," or - basically - "every set of curly braces is a new scope."
		<br />
    There are essentially two situations where block scope becomes useful: when using arrow function expressions, and when writing loops. Using the former is itself inadvisable for many reasons, but arrow function expressions have a handful of extremely strong use cases. For these, block-scoped <code>let</code> variables are very useful. Similarly, writing loops is generally inadvisable, but in those situations when a loop is the best tool available, once again <code>let</code>'s block scoping is invaluable.
    <br />
    Note what's being asserted here: if you need to send a particular message to the next reader, that's the time to opt into a new scoping mechanism. If the new scoping mechanism isn't necessary or if - as is the case in the vast majority of cases - lexical and block scoping are identical, sending a confusing message to the next reader is wrong.
    <br />
		The <code>let</code> keyword is a signaling mechanism to the next reader that there is a conflict between the scope on the current line, and the lexical scope it may inherit. <code>let</code> is a flashing red light with a klaxon that says <strong>"BLOCK SCOPING IS NEEDED HERE FOR SOME REASON! PAY ATTENTION!"</strong>.
		<br />
		<code>let</code> is a for a small handful of edge cases, and <strong>only</strong> for these edge cases.
	</p>
	<h2>Rarified <code>const</code></h2>
  <p>
    Finally we arrive at <code>const</code>. A shorthand for the English word "constant." There is almost nothing in programming that is constant. There's even less in JavaScript that is constant, because <code>const</code> isn't even the thinnest veneer of constancy at all.
		<br />
    <strong>Any</strong> use of <code>const</code> is explicitly a lie to future readers. There is no guarantee of constancy in any form.
    <br />
    That said, language is imprecise, and "constant" is a pretty well-understood word. It would be a shame to be unable to use it. We also have plenty of wise advice against <em>ever</em> modifying built-in objects (the only way to break constancy for boxed primitives) , so with our fingers crossed we can decide to use <code>const</code> for a small set of variable declarations.
    <br />
    The only acceptable time to tell future readers of your code that something is constant is when you can be reasonably sure that on a reasonable timescale (say, a few weeks) the thing in the variable won't be any different.
    <br />
    This is the definition of constant, and when the programming language trades on the familiarity of that word, it must also depend on the definition of it. Here are a few examples of constants:
  </p>
  <pre><code>const AVOGADROS_NUMBER = 6.02214076E23;
const PI_TO_TWENTY = 3.1415926535897932384;
const ONE_HOUR_IN_MS = 3600000;
const THE_BOSS_MAN_ID = 42;
const COMPANY_PHONE = "123-555-6789";</code></pre>
  <p>
    Remember: every character in code is a form of communication to the next reader. Therefore, the only valid time to communicate that something is constant is when the declared variable is holding something that's constant in the real world that the software is modeling.
  </p>
  <p>
    So to recap our options for declaring a variable:
  </p>
  <ol>
    <li><code>var</code> is king. Practically everything is variable. The default scope in JavaScript is lexical scope. Reach for <code>var</code> first to allow future readers to lean on that default and understand that you have no special case scenario.</li>
    <li><code>let</code> is for those special cases. <code>let</code> is a caution sign at the side of the road for the next reader: "This code needed to opt into a special case of block scoping, so pay attention!" In general terms, you should only use <code>let</code> in loops and arrow function expressions.</li>
    <li><code>const</code> is for telling the next reader that a value is a constant. It's for signaling that you want the reader to opt into a mental model that corresponds with the world around them, just like they always do for everything else. <code>const</code> says to the reader: "This is something you can understand just by reading what's 'on the tin.' It's a constant!"</li>
  </ol>
  <p>
    It's by no stretch of the imagination a perfect measure, but this rule is very close: <strong>if you're placing anything other than a primitive String or a primitive Number on the right-hand side of a <code>const</code> declaration, you're writing inhuman code.</strong>
  </p>
</section>
<section>
  <h2>But What About Some Other Mental Model!?</h2>
  <p>
    A mental model other than the one everyone is using for everything, all the time? The one where everything seen - including source code - is interpreted through a human being's experience of the real world?
    <br />
    Indeed, let's put aside that mental model and - purely for the sake of the exercise - pretend that the standard human cognitive model may not apply.
    <br />
    I have been subjected to a number of explanations for why <code>const</code> should be used more frequently. Of these, the only two that made any internal sense were these:
  </p>
  <ul>
    <li><code>const</code> isn't about value mutability, so acting like the assignment to a <code>const</code> is a constant value is the real lie.</li>
    <li>The value of <code>const</code> is signaling that you don't intend to re-assign the variable binding at a later time.</li>
	</ul>
	<h3>It's not about value mutability, it's about modeling the real world</h3>
  <p>
    The first of these is the most trivial: Indeed, practically nothing in JavaScript is immutable (with the exception of actual primitives, but even these are dynamically <a href="https://stackoverflow.com/questions/13055/what-is-boxing-and-unboxing-and-what-are-the-trade-offs">boxed and unboxed</a> at any time, so it's very difficult to get a truly primitive value orphaned from any possibility of being a higher-order type). Because practically nothing is immutable, it's entirely not useful to think about <code>const</code> in terms of value mutability.
    <br />
    The way to think about <code>const</code>, like the way we should be thinking about all things, is: "What is the human who will be reading this next going to think when they see this?" We all use a litany of mental shortcuts and cognitive abbreviations to get through our lives, and <code>const</code> will always be expanded by the brain to "constant." It's just missing 3 letters!
    <br />
    The way to think about <code>const</code> is to model it after the real world that the software is modeling, and that the human reading the code lives in. This is the model for <code>const</code>, not value mutability.
    <br />
    Put more succinctly: <em>The code is irrelevant! It's the people that matter!</em>
	</p>
	<h3>Programming has never had - and should never have - an intent-based programming paradigm</h3>
  <p>
    The second of these explanations is far more nuanced, and as with all things more nuanced, makes me much angrier.
    <br />
    There is no place in software development for communicating some other-worldly future intent. Indeed, this tenet has been concrete in software for practically all the time that software has existed.
    <br />
    There is no mechanism to magically push an author's intent onto some poor reader's mental stack, because doing so is an absolutely horrific thing to do to another person. The reason software works is because readers don't need to maintain a mental registry of which intent was registered in which order, and whether that intent has been fulfilled yet.
	</p>
	<h4><code>const</code>-infers-intent is practically unfalsifiable</h4>
	<p>
    Especially - <strong>especially</strong> - when there's no falsifiable statement for that intent. If <code>const</code> signals "intent to not re-assign the binding," the only way to determine if that intent is fulfilled is to read the entirety of the source code from top to bottom. Only then - and only at that moment - can you say the intent was fulfilled. Any changes to the source at all will obviate your verification completely and you'll have to start all over.
    <br />
    The only useful measure of whether a variable is intended to never be re-assigned is <strong>never re-assigning the variable</strong>.
	</p>
	<h4>The madness of <code>const</code></h4>
	<blockquote>
		There's zero concept of a pop-fly ball that will come crashing down into the code randomly at some later time.
	</blockquote>
	<p>
    The madness here is this: saying that <code>const</code> communicates intent implies that the wider world of development should introduce an entirely new mental paradigm: the intent-based program.
    <br />
    Nowhere in all of JavaScript (perhaps all of programming?) have we ever done anything remotely like this. Code is always about <strong>now</strong>. What is the code doing <em>now</em>. It's the only thing that matters. Even code that inherently deals with the future state of things - like Observables and Promises - does so by interacting with an object... now. A stream of data or a promise of a response is created immediately, and dealt with immediately. The author binds listeners to data entering the stream, or they handle different states of the promised response. In some cases, the author hands the response object off to some other set of code for it to handle.
    <br />
    In every case, some <em>future behavior</em> is dealt with right now in the code. There's zero concept of a pop-fly ball that will come crashing down into the code randomly at some later time. This is what the idea that <code>const</code>-as-intent implies. It's an entirely new and baffling mindset that expects a reader to ingest a line of code as "future intent" and then - at an unspecified and unconnected moment later - recall the intent.
    <br />
    This is madness, and I refuse to acknowledge ideas as valid if they make software less readable, and less comprehendable. If intent-based programming is the future of our world, may God have mercy on our souls.
  </p>
</section>
<section>
	<h2>Once more, for the folks in the back</h2>
	<ol>
		<li>The <strong>intent of the author</strong> is irrelevant. Programming doesn't deal with what your intent is or was.</li>
		<li>Value mutability is irrelevant. <code>const</code> should be used to match the mental model of constant things in the real world.</li>
		<li><code>var</code> should be the default variable declaration choice.</li>
		<li><code>let</code> should be used when certain special cases are encountered, like arrow function expressions and loops (use both with caution!). Use <code>let</code> to tell the next reader that block scoping is <strong>different</strong> from lexical scoping, and it's needed right here!</li>
		<li><code>const</code> should be used to pull magic numbers (like IDs, range limits, special numbers, etc.) or repeatable primitive strings (like a phone number or a special phrase) out of the code. Anything "constant" in the real world can be <code>const</code> in code.</li>
	</ol>
</section>
